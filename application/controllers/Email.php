<?php 
class Email extends MY_Controller {
    public function __construct() {
		parent::__construct();
        if (!$this->session->login){
            redirect('Login');
        }
        // if ($this->session->subscribe){
        //     $this->session->set_flashdata('sukseslogin', 'Anda Sudah Subscribe');
        //     redirect('Home');
        // }
    }

    public function index(){
        $this->template('Email/Form');
        // $this->load->view('Email/emailtemplate');
    }

    public function subscribe(){
        $this->form_validation->set_rules('username','Username','required');
        $this->form_validation->set_rules('email','Email','required|valid_email');
        $to = $this->input->post('email',true);
        $nama = $this->input->post('username',true);
        $judul = 'Langganan Majalah!';
        $msg = 'Masa berlangganan Anda akan berjalan mulai Hari ini';
        
        $datasession = [
            'subscribe' => true,
            'nama' => $post['username'],
            'email' => $post['email'],
            'sesi' => session_id()
        ];
        $this->session->set_userdata($datasession);


        if($this->form_validation->run()==true){
            $subscribe = $this->send_email($to,$judul,$msg,$nama);
            if($subscribe){
	            $this->session->set_flashdata('suksessubscribe', 'Selamat Anda Sudah Berlangganan!');
	            redirect(site_url('Home'));

        	}else{
        		$this->session->set_flashdata('gagalsubscribe', 'Anda Gagal Berlangganan,Silahkan Coba Lagi');
        		$this->template('Email/Form');
        	}
        }else{
            $this->template('Email/Form');
        }
    
    }
}