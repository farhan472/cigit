<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profil extends MY_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('Profil_m');
        if (!$this->session->login){
            redirect('Login');
        }
    } 
    public function index(){
        $data['judul'] = 'Profil';
        $username = $this->session->userdata('nama');
        $data = $this->db->get_where('user',['username' => $username])->row_array();
        // $foto['file_name'] = 'blank_profile_picture.png';
        $this->template('profil/profil',$data);
        // var_dump($foto);
    }

    public function tampilan($filename){
        $foto = './assets/profil/'.$filename;
        
        if(file_exists($foto)){
            $this->load->helper('download');
            force_download($filename, file_get_contents($foto));
            exit;
        }
    }

    public function edit($id){
        $data = $this->Profil_m->getuser($id)->row_array();
        $data['judul'] = 'Edit User';
		$this->template('Profil/Edit', $data);
	}

    public function edit_success(){
        $this->form_validation->set_rules('username','Username','required|min_length[4]|max_length[100]');
        $this->form_validation->set_rules('email','Email','required|min_length[4]|max_length[24]|valid_email');
        $post = $this->input->post();

        if($this->form_validation->run()==true)
        {
            $data = [
                'username' => $this->input->post('username'),
                // 'password' => password_hash($this->input->post('password'),PASSWORD_DEFAULT),
                'posisi' => 'user',
                'email' => $this->input->post('email'),
                'foto' => $_FILES['foto']['name']
            ];

            $uploadfoto = $this->uploadfoto($_FILES['foto']);
            $id = $this->input->post('id');
            $this->Profil_m->edituser($id,$data);
            redirect('Profil');
        }
        else
        {
            $this->index();
        }
    }

    private function uploadfoto($foto){
        $dir = './assets/profil/';
        $dir = str_replace(array("\\", "//"), "/", $dir);
        $config['upload_path'] = $dir;
        $config['allowed_types'] = 'jpg|png|gif';
        // $config['max_size'] = '15000';
        // $config['max_width'] = '1024';
        // $config['max_height'] = '768';
        $config['file_name'] = $foto['name'];
        $config['overwrite'] = true;
        $config['encrypt_name'] = false;

        $this->load->library('upload', $config);
        

        if ($this->upload->do_upload('foto')) {
            $upload = $this->upload->data();

            $config['image_library'] = 'gd2';
            $config['source_image'] = $dir."/".$upload_foto['filename'];
            $config['create_thumb'] = FALSE;
            $config['maintain_ratio'] = TRUE;
            $config['quality'] = '60%';
            $config['width'] = 150;
            $config['height'] = 150;
            $config['new_image'] = $dir."/".$upload_foto['file_name'];

            $this->load->library('image_lib',$config);
            
            $this->image_lib->resize();
            $this->image_lib->clear();
        }else{
            return $this->upload->display_errors();
        }
        
    }
    
    // public function image(){
    //     $config['image_library'] = 'gd2'
    //     $config['source_image'] = './assets/profil'
    //     $config['create_thumb'] = TRUE;
    //     $config['maintain_ratio'] = TRUE;
    //     $config['width'] = 150;
    //     $config['height'] = 150;

    // $this->load->library('image_lib',$config);

    // $this->image_lib->resize();
    // }


}