<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Karyawan extends MY_Controller{
    public function __construct() {
		parent::__construct();
        $this->load->model('Karyawan_m');
        $this->encryption->initialize(
            array(
            'cipher' => 'aes-192',
            'mode' => 'ofb',
            'key' => 'PasswordFish9909?')
        );
        if (!$this->session->login){
            redirect('Login');
            
        }
        // if ($this->session->register){
        //     $this->session->set_flashdata('sukseslogin', 'Anda Sudah Mendaftar');
        //     redirect('Home');
        // }
    }
    
    public function index(){
        $data['judul'] = 'Daftar Pelamar';
        $data['Karyawan'] = $this->Karyawan_m->getall()->result_array();
        $this->template('Karyawan/data_karyawan',$data);
    }

    public function daftar(){
        $data['judul'] = 'Daftar Pelamar';
        $this->template('Karyawan/Form',$data);
    }

    public function daftar_success(){
        $this->form_validation->set_rules('firstname','First Name','required|min_length[4]|max_length[100]|trim');
        $this->form_validation->set_rules('lastname','Last Name','required|min_length[4]|max_length[100]|trim');
        $this->form_validation->set_rules('tgl_lahir','Tanggal Lahir','required|trim');
        $this->form_validation->set_rules('alamat','Alamat','required|min_length[4]|max_length[100]|trim');
        $this->form_validation->set_rules('hobi','Hobi','required|trim');
        $this->form_validation->set_rules('deskripsi','Deskripsi','required|trim');
        $this->form_validation->set_rules('jenis_kelamin','Jenis Kelamin','required|trim');
        $this->form_validation->set_rules('no_telp','Nomor Telepon','required|trim');
        // $this->form_validation->set_rules('email','Email','required|trim');
        $this->form_validation->set_rules('email','Email','required|valid_email|is_unique[karyawan.email]|trim',['is_unique' => 'This Email has already registered']);
        // $this->form_validation->set_rules('foto','Foto','');
        // $this->form_validation->set_rules('file','File','');
        $post = $this->input->post();
        $db = $this->Karyawan_m->getall()->result();
        $emaildekrip = [];
        foreach($db as $mail){
            $dataemail = $mail->email;
            $emaildekrip[] = $this->encryption->decrypt($dataemail);
        } // ini nge dekrip data email
        $validasi = in_array($post['email'],$emaildekrip);
        // echo "<pre>";
        // var_dump($validasi);
        // die;
        if($this->form_validation->run()==true && $validasi == false){
            $data = [
            'firstname' => $post['firstname'],
            'lastname' => $post['lastname'],
            'tgl_lahir' => $post['tgl_lahir'],
            'alamat' => $post['alamat'],
            'hobi' => $post['hobi'],
            'deskripsi' => $post['deskripsi'],
            'jenis_kelamin' => $post['jenis_kelamin'],
            'no_telp' => $post['no_telp'],
            'email' => $post['email']
            ];

            $input = [
                'firstname' => $post['firstname'],
                'lastname' => $post['lastname'],
                'tanggal_lahir' => $post['tgl_lahir'],
                'alamat' => $post['alamat'],
                'foto' => $_FILES['foto']['name'],
                'file' => $_FILES['file']['name'],
                'hobi' => $post['hobi'],
                'deskripsi' => $post['deskripsi'],
                'jenis_kelamin' => $post['jenis_kelamin'],
                'no_telp' => $post['no_telp'],
                'email' => $this->encryption->encrypt($post['email']),
            ];
            
            //$uploadfoto = $this->uploadfoto($_FILES['foto']);
            if (isset($_FILES['foto'])) {
                $foto = $_FILES['foto'];
                $upload = $this->uploadfoto($foto);
                $data['file_name'] = $upload['file_name'];
                $data['orig_name'] = $upload['orig_name'];
            }
            $uploadfile = $this->uploadfile($_FILES['file']);


            $datasession = [
                'register' => true,
                'firstname' => $post['firstname'],
                'lastname' => $post['lastname'],
                'sesi' => session_id()
            ];
            
            $this->session->set_userdata($datasession);

            
            // $penerima= $this->session->userdata('email');
            // $subject ='Selamat Anda Sudah Mendaftar' ;
            // $pesan = 'Tunggu Panggilan Perusahaan Anda' ;
            // $nama=  $this->session->userdata('firstname');
            // $attach = $uploadfile['full_path'];
            // if($this->send_email($penerima,$subject,$pesan,$nama,$attach)){                 
            //     if(delete_files($uploadfile['file_path'])){                     //untuk mendelete setelah file selesai di send email 
            //         $this->session->set_flashdata('sukseslogin', 'Lamaran terkirim');
            //     } else {
            //         $this->session->set_flashdata('error', 'Lamaran gagal terkirim');
            //         redirect('Karyawan/daftar');
            //     }
            // }
            $this->Karyawan_m->create($input);
            $data['judul'] = 'Register Success';
            $this->template('Karyawan/daftar_success',$data);
        }else{
            $data = [
                'firstname' => $this->input->post('firstname'),
                'lastname' => $this->input->post('lastname'),
                'tgl_lahir' => $this->input->post('tgl_lahir'),
                'alamat' => $this->input->post('alamat'),
                'hobi' => $this->input->post('hobi'),
                'deskripsi' => $this->input->post('deskripsi'),
                'jenis_kelamin' => $this->input->post('jenis_kelamin'),
                'no_telp' => $this->input->post('no_telp'),
                'email' =>  $this->input->post('email')
            ];
            $data['judul'] = 'Register';
            $this->session->set_flashdata('error', 'Email Sudah di pakai');
            $this->template('Karyawan/Form',$data);
        }
    }
    
    public function update($id){
        $data['judul'] = 'Daftar Pelamar';
        $id_asli = $this->encryption->decrypt(str_replace(['garing','samadengan','ples'],['/','=','+'],$id));
        $data['karyawan'] = $this->Karyawan_m->detail($id_asli);
        $this->template('Karyawan/update',$data);
    }

    
    public function update_success(){
        $this->form_validation->set_rules('firstname','First Name','required|min_length[4]|max_length[100]|trim');
        $this->form_validation->set_rules('lastname','Last Name','required|min_length[4]|max_length[100]|trim');
        $this->form_validation->set_rules('tgl_lahir','Tanggal Lahir','required|trim');
        $this->form_validation->set_rules('alamat','Alamat','required|min_length[4]|max_length[100]|trim');
        $this->form_validation->set_rules('hobi','Hobi','required|trim');
        $this->form_validation->set_rules('deskripsi','Deskripsi','required|trim');
        $this->form_validation->set_rules('jenis_kelamin','Jenis Kelamin','required|trim');
        $this->form_validation->set_rules('no_telp','Nomor Telepon','required|trim');
        $this->form_validation->set_rules('email','Email','required|valid_email|is_unique[karyawan.email]|trim',['is_unique' => 'This Email has already registered']);
        // $this->form_validation->set_rules('foto','Foto','');
        // $this->form_validation->set_rules('file','File','');
        $id = $this->encryption->encrypt($this->input->post('id'));
        $post = $this->input->post();
        $db = $this->Karyawan_m->getall()->result();
        $emaildekrip = [];
        foreach($db as $mail){
            $dataemail = $mail->email;
            $emaildekrip[] = $this->encryption->decrypt($dataemail);
        } // ini nge dekrip data email
        $validasi = in_array($post['email'],$emaildekrip);
        if($this->form_validation->run()==true && $validasi == false){
            $data = [
            'firstname' => $post['firstname'],
            'lastname' => $post['lastname'],
            'tgl_lahir' => $post['tgl_lahir'],
            'alamat' => $post['alamat'],
            'hobi' => $post['hobi'],
            'deskripsi' => $post['deskripsi'],
            'jenis_kelamin' => $post['jenis_kelamin'],
            'no_telp' => $post['no_telp'],
            'email' => $post['email']
            ];

            $input = [
                'firstname' => $post['firstname'],
                'lastname' => $post['lastname'],
                'tanggal_lahir' => $post['tgl_lahir'],
                'alamat' => $post['alamat'],
                'foto' => $_FILES['foto']['name'],
                'file' => $_FILES['file']['name'],
                'hobi' => $post['hobi'],
                'deskripsi' => $post['deskripsi'],
                'jenis_kelamin' => $post['jenis_kelamin'],
                'no_telp' => $post['no_telp'],
                'email' => $this->encryption->encrypt($post['email']),
            ];
            
            //$uploadfoto = $this->uploadfoto($_FILES['foto']);
            if (isset($_FILES['foto'])) {
                $foto = $_FILES['foto'];
                $upload = $this->uploadfoto($foto);
                $data['file_name'] = $upload['file_name'];
                $data['orig_name'] = $upload['orig_name'];
            }
            $uploadfile = $this->uploadfile($_FILES['file']);


            // $datasession = [
            //     'register' => true,
            //     'firstname' => $post['firstname'],
            //     'lastname' => $post['lastname'],
            //     'sesi' => session_id()
            // ];
            // $this->session->set_userdata($datasession);

            
            // $penerima= $this->session->userdata('email');
            // $subject ='Selamat Anda Sudah Mendaftar' ;
            // $pesan = 'Tunggu Panggilan Perusahaan Anda' ;
            // $nama=  $this->session->userdata('firstname');
            // $attach = $uploadfile['full_path'];
            // if($this->send_email($penerima,$subject,$pesan,$nama,$attach)){                 
            //     if(delete_files($uploadfile['file_path'])){                     //untuk mendelete setelah file selesai di send email 
            //         $this->session->set_flashdata('sukseslogin', 'Lamaran terkirim');
            //     } else {
            //         $this->session->set_flashdata('error', 'Lamaran gagal terkirim');
            //         redirect('Karyawan/daftar');
            //     }
            // }
            // $id = $this->input->post('id');
            $id_asli = $this->encryption->decrypt(str_replace(['garing','samadengan','ples'],['/','=','+'],$id));
            $this->Karyawan_m->updatekaryawan($id_asli,$input);
            $data['judul'] = 'Register Success';
            // $this->template('Karyawan/daftar_success',$data);
            redirect('Karyawan/index');
        }else{
            // $data = [
            //     'firstname' => $this->input->post('firstname'),
            //     'lastname' => $this->input->post('lastname'),
            //     'tgl_lahir' => $this->input->post('tgl_lahir'),
            //     'alamat' => $this->input->post('alamat'),
            //     'hobi' => $this->input->post('hobi'),
            //     'deskripsi' => $this->input->post('deskripsi'),
            //     'jenis_kelamin' => $this->input->post('jenis_kelamin'),
            //     'no_telp' => $this->input->post('no_telp'),
            //     'email' => $this->input->post('email')
            // ];
            // $data['judul'] = 'Register';
            // $this->template('Karyawan/Form',$data);
            $this->session->set_flashdata('error', 'Email Sudah di pakai');
            $this->update($id);
        }
    }

    public function downloadfoto($filename,$origname){
        $foto = './assets/foto/'.$filename;

        if(file_exists($foto)){
            $this->load->helper('download');
            force_download($origname, file_get_contents($foto));
            exit;
        }
    }

    // public function downloadfile($filename,$origname){
    //     $file = './assets/file/'.$filename;

    //     if(file_exists($file)){
    //         $this->load->helper('download');
    //         force_download($origname, file_get_contents($file));
    //         exit;
    //     }
    // }
    private function uploadfile($file){
        $dir = APPPATH."upload/";
        $dir = str_replace(array("\\", "//"), "/", $dir);
        $config['upload_path'] = $dir;
        $config['allowed_types'] = 'doc|docx|pdf';
        $config['overwrite'] = true;

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('file')) {
            return $this->upload->data();
        }else{
            return $this->upload->display_errors();
        }
         
    } 

    private function uploadfoto($foto){
        $dir = './assets/foto/';
        $dir = str_replace(array("\\", "//"), "/", $dir);
        $config['upload_path'] = $dir;
        $config['allowed_types'] = 'jpg|png|gif';
        $config['max_size'] = '15000';
        // $config['max_width'] = '1024';
        // $config['max_height'] = '768';
        $config['file_name'] = $foto['name'];
        $config['overwrite'] = true;
        $config['encrypt_name'] = false;

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('foto')) {
            return $this->upload->data();
        }else{
            return $this->upload->display_errors();
        }
        
    }
    
    public function detail($id){
        $data['judul'] = 'Detail Data Siswa';
        $id_asli = $this->encryption->decrypt(str_replace(['garing','samadengan','ples'],['/','=','+'],$id));
        $data['Karyawan'] = $this->Karyawan_m->detail($id_asli);
        $this->template('Karyawan/detail_karyawan',$data);
     }
  
     // public function update() {
     //    $id = $this->uri->segment(3);
     //    $update = $this->Siswa_m->update($id);
     //    if($update) {
     //       echo 'Berhasil Mengubah Data';
     //    } else {
     //       echo 'Gagal Mengubah Data';
     //    }
     // }
  
     public function tampilan($filename){
        $foto = './assets/foto/'.$filename;
        
        if(file_exists($foto)){
            $this->load->helper('download');
            force_download($filename, file_get_contents($foto));
            exit;
        }
    }
  
     public function delete($id,$foto) {
     $id_asli = $this->encryption->decrypt(str_replace(['garing','samadengan','ples'],['/','=','+'],$id));
     $this->Karyawan_m->delete($id_asli);
     
     $filepath = './assets/foto/'.$foto;
     $filepath = str_replace(array("\\","//"),"/",$filepath);
     unlink($filepath);
     redirect('Karyawan/index');
     }
    
}