<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->model('Login_m');
        if ($this->session->login){
            redirect('Home');
        }
    }
    // public function index(){
    //     $data['judul'] = 'Login';
    //     $this->load->view('templates/header',$data);
    //     $this->load->view('Form/Login');
    //     $this->load->view('templates/footer');
    // }

    // public function login_success(){
    //     $this->form_validation->set_rules('username','Username','required|min_length[4]|max_length[255]');
    //     $this->form_validation->set_rules('password','Password','required|min_length[4]|max_length[20]');

    //     if ($this->form_validation->run()==true)
    //     {
    //     $data['nama'] = $this->input->post('username');
    //     $data['judul'] = 'Login Berhasil';
    //     $this->load->view('templates/header',$data);
    //     $this->load->view('form/Login_success',$data);
    //     $this->load->view('templates/footer');
    //     }
        
    //     else {
    //         $data['judul'] = 'Login';
    //         $this->load->view('templates/header',$data);
    //         $this->load->view('form/Login');
    //         $this->load->view('templates/footer');
    //     }
    // }
    // public function __construct() {
    //     parent::__construct();
    //     $login = $this->input->cookie('username');

    //     if(!isset($login)){
    //         redirect('/login');
    //     }
    // }   

    public function index(){
        // $login = $this->input->cookie('username');
        $set['value'] = get_cookie('username');
        $set['password'] = get_cookie('password');
        
        $data['judul'] = 'Login';
        $this->load->view('templates/header',$data);
        $this->load->view('Form/Login',$set);
        $this->load->view('templates/footer');
    }
    
    public function login_success(){
        $this->form_validation->set_rules('username' , 'Username' , 'required|min_length[4]|max_length[20]');
        $this->form_validation->set_rules('password' , 'Password' , 'required|min_length[4]|max_length[20]');

        if ($this->form_validation->run()==true)
        {      
            
                $username = $this->input->post('username');
                $password = $this->input->post('password');
                
                $databaseuser = $this->db->get_where('user', ['username' => $username])->row_array();
                if(password_verify($password, $databaseuser['password']))
                {
                    $datasession = [
                        'login' =>TRUE,
                        'nama' => $databaseuser['username'],
                        'posisi' => $databaseuser['posisi'],
                        'sesi' => session_id(),
                        'email' => $databaseuser['email']
                    ];
                    $this->session->set_userdata($datasession);
                    
                    if($this->agent->is_mobile())
                    {
                        $device = $this->agent->mobile();
                    }
                    elseif ($this->agent->is_robot())
                    {
                            $device = $this->agent->robot();
                    }
                    elseif ($this->agent->is_browser())
                    {
                            $device = 'Komputer/Laptop';
                    }
                    else
                    {
                            $device = 'Unidentified User Agent';
                    }
                    
                    $input = [
                        'waktu_login' => date('Y-m-d H:i:s'),
                        'id_user' => $databaseuser['id'],
                        'os' => $this->agent->platform(),
                        'device' => $device,
                        'browser' => $this->agent->browser().' '.$this->agent->version(),
                        'ip' => $this->input->ip_address(),
                    ];
                    
                    $this->Login_m->insertlog($input);
                    $name = $this->session->nama;
                    $this->session->set_flashdata('sukseslogin', 'Selamat Datang '.$name);
                    redirect('Home/index');
                }else{
                    $this->session->set_flashdata('gagallogin', 'Password salah');
                    redirect('Login/index');
                    }
        // date_default_timezone_set('Asia/Jakarta');
        // $datestring = '%d-%m-%Y %H:%i:%s';
        // $time = time();
        // $expired = time()+'3600';
        // $set['expired'] = mdate($datestring,$expired);
        // $set['waktu'] = mdate($datestring,$time);        
        }
        else {
            $set['value'] = $this->input->post('username');
            $data['judul'] = 'Login';
            $this->load->view('templates/header',$data);
            $this->load->view('form/Login',$set);
            $this->load->view('templates/footer');
        }
    }

    

    public function check(){
        //untuk mendeteksi expired dari cookie 
        $login = $this->input->cookie('username');
        if(!isset($login)){
            redirect('/login');
        }
        
        date_default_timezone_set('Asia/Jakarta');
        $datestring = '%d-%m-%Y %H:%i:%s';
        $time = time();
        $expired = time()+'100';
        $set['expired'] = mdate($datestring,$expired);
        $set['waktu'] = mdate($datestring,$time);
        $set['name'] = get_cookie('username');
        $set['value'] = get_cookie('username');
        // $set['value'] = $_COOKIE[$cookie_value];
        // $set['name'] = $_COOKIE[$cookie_name];
        $data['judul'] = 'Check Cookie';
        $this->load->view('templates/header',$data);
        $this->load->view('templates/navbar');
        $this->load->view('form/cek',$set);
        $this->load->view('templates/footer');
    }

    public function delete(){
        delete_cookie('username');
        delete_cookie('password');
        delete_cookie('nama');
        delete_cookie('email');
        delete_cookie('notelp');
        delete_cookie('tlahir');
        delete_cookie('jenis_kelamin');
        delete_cookie('alamat');
        delete_cookie('agama');
        delete_cookie('tinggi');
        delete_cookie('berat');
        echo 'Berhasil Menghapus Cookie Nama';
        redirect('/Login/index');
    }
}?>