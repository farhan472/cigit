<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jurusan extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('form');
		$this->load->model('Jurusan_m');
		if (!$this->session->login){
			redirect('Login');
		}
	}
	
	public function index(){
	$this->template('jurusan/jurusan_v');
	}

	public function data_jurusan()
	{
		$get = $this->input->get();
		
		$limit = !empty($get['limit']) ? $get['limit'] : 0;
		$search = !empty($get['search']) ? $get['search'] : "";
		$offset = !empty($get['offset']) ? $get['offset'] : 0;
		$sort = !empty($get['sort']) ? $get['sort'] : "kode_jurusan";
		$order = !empty($get['order']) ? $get['order'] : "asc";
		
		$data['total'] = $this->Jurusan_m->getJurusan("", 0, 0, $search)->num_rows();
		
		$this->db->order_by($sort, $order);
		$data['rows'] = $this->Jurusan_m->getjurusan("",$limit,$offset,$search)->result_array();

		echo json_encode($data);
	}
	// $this->load->library('pagination');
	// $limit = !empty($this->input->get("limit")) ? $this->input->get("limit") : 5;
	// $limit = $limit == "all" ? 0 : $limit;
	// $search = !empty($this->input->get('search')) ? $this->input->get("search") : "";
	// $sorting = !empty($this->input->get('sort')) ? $this->input->get('sort') : "";
	// $data['data'] = $this->Jurusan_m->getJurusan("", 0, 0, $search)->result_array();
	// // echo $this->db->last_query();
	// $config['base_url'] = site_url('jurusan').'?sort='.$sorting.'&limit='.$limit.'&search='.$search;
	// $config['total_rows'] = count($data['data']);
	// $config['per_page'] = $limit;
	// $config['full_tag_open'] = '<p>';
	// $config['full_tag_close'] = '</p>';
	// $config['num_tag_open'] = "<span style='padding:5px'>";
	// $config['num_tag_close'] = "</span>";
	// $config['page_query_string'] = true;
	

	// $this->pagination->initialize($config);
	// $num_link = $this->input->get('per_page');
	// $data['num_link'] = $num_link;
	// if(!empty($sorting)){
	// 	$sorting = urldecode($sorting);
	// 	$sorting = explode(",", $sorting);
	// 	$this->db->order_by($sorting[0], $sorting[1]);
	// }
	// $data['data'] =  $this->Jurusan_m->getJurusan("", $limit, $num_link, $search)->result_array();
	// $data['search'] = $search;
	// $data['limit'] = $limit;
	// //$data['limit_options'] = array(5=>5, 7=>7, 'all'=>"Semua");
	// $data['sorting'] = $sorting;
	// $data['pagination'] = $this->pagination->create_links();
	
	// $this->template('jurusan/jurusan_v', $data);
	
	public function test()
	{
		
		$data = $this->Jurusan_m->getJurusan()->result();

		foreach ($data as $k => $v) {
			echo "kode jurusan :". $v->kode_jurusan;
			echo "<br/>";
			echo "nama jurusan :". $v->nama_jurusan;
			echo "<br/>";
			echo "======================================";
			echo "<br/>";
		}

		// echo "kode jurusan :". $data->kode_jurusan;
		// echo "<br/>";
		// echo "nama jurusan :". $data->nama_jurusan;
		// echo "<br/>";
		// echo "=====================================";
		// echo "<br/>";
		
		
	}

	public function tambah(){
		$this->template('jurusan/tambah_jurusan_v');
	}

	public function submit_tambah(){

        $this->load->library('form_validation');

        $this->form_validation->set_rules('kode_jurusan', 'Kode Jurusan', 'required');
		$this->form_validation->set_rules('nama_jurusan', 'Nama Jurusan', 'required');

		$post = $this->input->post();

        if ($this->form_validation->run() == FALSE)
        {
            $this->template('jurusan/tambah_jurusan_v');
        }
        else
        {
        	$data = ["kode_jurusan" => $post["kode_jurusan"], "nama_jurusan" => $post["nama_jurusan"], "created_datetime" => date('Y-m-d H:i:s') ];
        	$insert = $this->Jurusan_m->insertJurusan($data);

        	if($insert){

	            $this->session->set_flashdata('success_submit', 'Berhasil menambah jurusan');
	            redirect(site_url('Jurusan'));

        	}else{
        		$this->session->set_flashdata('error_submit', 'Gagal menambah jurusan');
        		$this->template('jurusan/tambah_jurusan_v');
        	}

        }
	}

	public function edit($id){

		$data['data'] = $this->Jurusan_m->getJurusan($id)->row_array();
		$this->template('jurusan/edit_jurusan_v', $data);

	}

	public function submit_edit(){

        $this->load->library('form_validation');

        $this->form_validation->set_rules('kode_jurusan', 'Kode Jurusan', 'required');
		$this->form_validation->set_rules('nama_jurusan', 'Nama Jurusan', 'required');

		$post = $this->input->post();
		$id = $post['id'];

        if ($this->form_validation->run() == FALSE)
        {
            
            $this->edit($id);
        }
        else
        {
        	$data = ["kode_jurusan" => $post["kode_jurusan"], "nama_jurusan" => $post["nama_jurusan"]];
        	
        	$update = $this->Jurusan_m->updateJurusan($id, $data);

        	if($update){

	            $this->session->set_flashdata('success_submit', 'Berhasil mengubah jurusan');
	            redirect(site_url('Jurusan'));

        	}else{
        		$this->session->set_flashdata('error_submit', 'Gagal mengubah jurusan');
        		
        		$this->edit($id);
        	}

        }
	}

	public function hapus($id){

		$delete = $this->Jurusan_m->deleteJurusan($id);

    	if($delete){

            $this->session->set_flashdata('success_submit', 'Berhasil menghapus jurusan');
            

    	}else{
    		$this->session->set_flashdata('error_submit', 'Gagal menghapus jurusan');
    		
    	}
    	redirect(site_url('Jurusan'));
	}



}

/* End of file Tambah_kelas.php */
/* Location: ./application/controllers/Tambah_kelas.php */