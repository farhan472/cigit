<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller{
    public function __construct() {
		parent::__construct();
        $this->load->model('Upload_m');
        $this->load->model('Siswa_m');
        $this->encryption->initialize(
            array(
            'cipher' => 'aes-192',
            'mode' => 'obf',
            'key' => 'PasswordFish9909?')
        );
        if (!$this->session->login){
            redirect('Login');
        }
    }
    Public function index(){

        $data['judul'] = 'Register';
        $this->load->view('templates/header',$data);
        $this->load->view('templates/navbar'); 
        $this->load->view('Form/Register');
        $this->load->view('templates/footer'); 
    }

    public function input()
	{   
        $this->form_validation->set_rules('nama','Nama','required|min_length[5]|max_length[255]');
        $this->form_validation->set_rules('email','Email','required|valid_email');
        $this->form_validation->set_rules('notelp','Nomor Telepon','required|numeric|min_length[10]|max_length[14]');
        $this->form_validation->set_rules('tlahir','Tanggal Lahir','required');
		$this->form_validation->set_rules('jenis_kelamin','Jenis Kelamin','required');
		$this->form_validation->set_rules('agama','Agama','required');
        $this->form_validation->set_rules('alamat','Alamat','required');
        $this->form_validation->set_rules('tinggi','Tinggi','required|numeric');
        $this->form_validation->set_rules('berat','berat','required|numeric');
        $post = $this->input->post();
        foreach(['nama','alamat'] as $key => $value) {
            $post[$value] = $this->security->xss_clean($post[$value]);
        }
        
        
        if($this->form_validation->run()==true)
        {

            $tinggi = $this->input->post('tinggi');
            $berat = $this->input->post('berat');

            $data['nama'] = $post['nama'];
            $data['email'] = $this->input->post('email');
            $data['notelp'] = $this->input->post('notelp');
            $data['tlahir'] = $this->input->post('tlahir');
            $data['jenis_kelamin'] = $this->input->post('jenis_kelamin');
            $data['agama'] = $this->input->post('agama');
            $data['alamat'] = $post['alamat'];
            $data['tinggi'] = $tinggi;
            $data['berat'] = $berat;
            $data['bmi'] = $this->Upload_m->bbideal($tinggi,$berat);

            $this->input->set_cookie('nama',$this->input->post('nama'),'100');
            $this->input->set_cookie('email',$this->input->post('email'),'100');
            $this->input->set_cookie('notelp',$this->input->post('notelp'),'100');
            $this->input->set_cookie('tlahir',$this->input->post('tlahir'),'100');
            $this->input->set_cookie('jenis_kelamin',$this->input->post('jenis_kelamin'),'100');
            $this->input->set_cookie('alamat',$this->input->post('alamat'),'100');
            $this->input->set_cookie('agama',$this->input->post('agama'),'100');
            $this->input->set_cookie('tinggi',$this->input->post('tinggi'),'100');
            $this->input->set_cookie('berat',$this->input->post('berat'),'100');

            $input = [
                "nama" => $this->input->post('nama'),
                "email" => $this->input->post('email'),
                "no_telp" => $this->input->post('notelp'),
                "tanggal_lahir" => $this->input->post('tlahir'),
                "jenis_kelamin" => $this->input->post('jenis_kelamin'),
                "alamat" => $this->input->post('alamat'),
                "agama" => $this->input->post('agama'),
                "tinggi" => $this->input->post('tinggi'),
                "berat" => $this->input->post('berat'),
                "foto" => $_FILES['file']['name']
            ];
            

            if (isset($_FILES['file'])) {
                $file = $_FILES['file'];
                $upload = $this->uploadfile($file);
                $data['file_name'] = $upload['file_name'];
                $data['orig_name'] = $upload['orig_name'];
            }
            $data['judul'] = 'Register';
            $this->Siswa_m->create($input);
            $this->load->view('templates/header',$data);
            $this->load->view('templates/navbar'); 
            $this->load->view('form/register_success',$data);
            $this->load->view('templates/footer'); 
        }
            else {
            $data['nama'] = $post['nama'];
            $data['email'] = $this->input->post('email');
            $data['notelp'] = $this->input->post('notelp');
            $data['tlahir'] = $this->input->post('tlahir');
            $data['jenis_kelamin'] = $this->input->post('jenis_kelamin');
            $data['agama'] = $this->input->post('agama');
            $data['alamat'] = $post['alamat'];
            $data['tinggi'] = $this->input->post('tinggi');
            $data['berat'] = $this->input->post('berat');
            //$data['foto'] = $_FILES['file']['name'];

            $data['judul'] = 'Register';
            $this->load->view('templates/header',$data);
            $this->load->view('templates/navbar'); 
            $this->load->view('Form/Register',$data);
            $this->load->view('templates/footer'); 
            }
    }

    public function update($id)
	{   
        $this->form_validation->set_rules('nama','Nama','required|min_length[5]|max_length[255]');
        $this->form_validation->set_rules('email','Email','required|valid_email');
        $this->form_validation->set_rules('notelp','Nomor Telepon','required|numeric|min_length[10]|max_length[14]');
        $this->form_validation->set_rules('tlahir','Tanggal Lahir','required');
		$this->form_validation->set_rules('jenis_kelamin','Jenis Kelamin','required');
		$this->form_validation->set_rules('agama','Agama','required');
        $this->form_validation->set_rules('alamat','Alamat','required');
        $this->form_validation->set_rules('tinggi','Tinggi','required|numeric');
        $this->form_validation->set_rules('berat','berat','required|numeric');
        
        $data['siswa'] = $this->Siswa_m->detail($id);
        $data['agama'] = ['Islam' , 'Kristen','Katholik','Hindu','Budha'];
        // $data['jenis_kelamin'] = ['laki-laki','perempuan'];


        if($this->form_validation->run()==true)
        {

            if (isset($_FILES['file'])) {
                $file = $_FILES['file'];
                $upload = $this->uploadfile($file);
                $data['file_name'] = $upload['file_name'];
                $data['orig_name'] = $upload['orig_name'];
            }

            $id = $this->input->post('id');
            $update = [
                "nama" => $this->input->post('nama'),
                "email" => $this->input->post('email'),
                "no_telp" => $this->input->post('notelp'),
                "tanggal_lahir" => $this->input->post('tlahir'),
                "jenis_kelamin" => $this->input->post('jenis_kelamin'),
                "alamat" => $this->input->post('alamat'),
                "agama" => $this->input->post('agama'),
                "tinggi" => $this->input->post('tinggi'),
                "berat" => $this->input->post('berat'),
                "foto" => $_FILES['file']['name']
            ];

            $this->Siswa_m->updatedatasiswa($id,$update);

            redirect('Siswa/index/');

            // $data['judul'] = 'Update Data Success';            
            // $this->load->view('templates/header',$data);
            // $this->load->view('form/register_success');
            // $this->load->view('templates/footer'); 
        }
            else {
            $data['judul'] = 'Update Data Siswa';
            $this->load->view('templates/header',$data);
            $this->load->view('templates/navbar'); 
            $this->load->view('Form/update_siswa',$data);
            $this->load->view('templates/footer'); 
            }
    }

    public function download($filename,$origname){
        $file = APPPATH."upload/".$filename;

        if(file_exists($file)){
            $this->load->helper('download');
            force_download($origname, file_get_contents($file));
            exit;
        }
    }

    private function uploadfile($file){
        $dir = APPPATH . "upload/";
        $dir = str_replace(array("\\", "//"), "/", $dir);
        $config['upload_path'] = $dir;
        $config['allowed_types'] = '*';
        $config['file_name'] = $file['name'];
        $config['overwrite'] = true;
        $config['encrypt_name'] = false;

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('file')) {
            return $this->upload->data();
        }else{
            return $this->upload->display_errors();
        }
        
    }   
}