<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cookie extends CI_Controller {


    public function index(){
        $nama = $this->input->cookie('nama');

        if($nama){
            echo $nama;
        }
        else{
            echo 'kuki nama kosong';
        }
    }

    public function create(){
        $set = array(
            'name' => 'nama',
            'value' => 'farhan',
            'expire' => '50'
        );

        $this->input->set_cookie($set);

        echo 'Berhasil Membuat Cookie Nama';
    }

    public function delete(){
        delete_cookie('nama');
        echo 'Berhasil Menghapus Cookie Nama';
        redirect('/cookie/index');
    }
}