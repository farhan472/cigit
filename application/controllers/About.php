<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller{
    public function __construct(){
        parent::__construct();
        if (!$this->session->login){
            redirect('Login');
        }
    }
    public function index($id='..',$nama='..'){
        $data['judul'] = 'About';
        $data['content'] = 'No ID '.$id.' bernama '.$nama;
        $this->load->view('templates/header',$data);
        $this->load->view('templates/Navbar');
        $this->load->view('About/index',$data);
        $this->load->view('templates/footer');
    }
    
}
?>