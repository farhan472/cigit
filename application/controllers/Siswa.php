<?php 
class Siswa extends MY_Controller {
   function __construct() {
      parent::__construct();
      $this->load->model('Siswa_m');
      if (!$this->session->login){
          redirect('Login');
      }
   }

   public function index() {
      $this->template('form/data_siswa');
   }

   public function data_siswa(){
      $get = $this->input->get();
		
		$limit = !empty($get['limit']) ? $get['limit'] : 0;
		$search = !empty($get['search']) ? $get['search'] : "";
		$offset = !empty($get['offset']) ? $get['offset'] : 0;
		$sort = !empty($get['sort']) ? $get['sort'] : "nama";
		$order = !empty($get['order']) ? $get['order'] : "asc";
		
		$data['total'] = $this->Siswa_m->getsiswa("", 0, 0, $search)->num_rows();
		
		$this->db->order_by($sort, $order);
		$data['rows'] = $this->Siswa_m->getsiswa("",$limit,$offset,$search)->result_array();
      
		echo json_encode($data);
   }

   // public function create() {
   //    $insert = $this->Siswa_m->create();
   //    if($insert) {
   //       echo 'Berhasil Menyimpan Data';
   //    } else {
   //       echo 'Gagal Menyimpan Data';
   //    }
   // }

   public function detail($id){
      $data['judul'] = 'Detail Data Siswa';
      $data['siswa'] = $this->Siswa_m->detail($id);
      $this->load->view('templates/header',$data);
      $this->load->view('templates/navbar');
      $this->load->view('form/detail_siswa',$data);
      $this->load->view('templates/footer');
   }

   // public function update() {
   //    $id = $this->uri->segment(3);
   //    $update = $this->Siswa_m->update($id);
   //    if($update) {
   //       echo 'Berhasil Mengubah Data';
   //    } else {
   //       echo 'Gagal Mengubah Data';
   //    }
   // }

   public function tampilan($filename){
      $foto = APPPATH."upload/".$filename;
      
      if(file_exists($foto)){
          $this->load->helper('download');
          force_download($filename, file_get_contents($foto));
          exit;
      }
  }

   public function delete($id,$foto) {
   //    $id = $this->uri->segment(3);
   //    $delete = $this->Siswa_m->delete($id);
   //    if($delete) {
   //       echo 'Berhasil Menghapus Data';
   //    } else {
   //       echo 'Gagal Menghapus Data';
   //    }
   $this->Siswa_m->delete($id);
   
   $filepath = APPPATH.'/upload'.'/'.$foto;
   $filepath = str_replace(array("\\","//"),"/",$filepath);
   unlink($filepath);
   redirect('Siswa/index');
   }
}