<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Form extends CI_Controller{
    public function __construct(){
        parent::__construct();
        if (!$this->session->login){
            redirect('Login');
        }
    }
    
    public function index(){
        $data['judul'] = 'Form';
        $this->load->view('templates/header',$data);
        $this->load->view('templates/navbar');
        $this->load->view('Form/index');
        $this->load->view('templates/footer');
    }
}


?>