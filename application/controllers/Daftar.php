<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Daftar extends MY_Controller {
    public function __construct(){
        parent ::__construct();
        $this->load->model('Daftar_m');

    }
    
    public function index(){
        $this->load->view('templates/header');
        $this->load->view('Daftar/daftar');
        $this->load->view('templates/footer');
    }

    public function input_user(){
        $this->form_validation->set_rules('username','Username','required|min_length[4]|max_length[100]');
        $this->form_validation->set_rules('password','Password','required|min_length[4]|max_length[24]');
        $post = $this->input->post();

        if($this->form_validation->run()==true)
        {
            $data = [
                'username' => $this->input->post('username'),
                'password' => password_hash($post["password"],PASSWORD_DEFAULT),
                'posisi' => 'user',
                'email' => $this->input->post('email'),
                'foto' => 'blank_profile_picture.png'
            ];

            $this->Daftar_m->input_user($data);
            redirect('Login');
        }
        else
        {
            $this->index();
        }
    }
}