<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beratbadan extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('Beratbadan_m');
     }
    
    public function index(){
        $this->load->view('templates/Main');
    }

    public function bbideal(){
		$b = 65; // berat 
        $t = 170; // tinggi
        // $data['hasil'] = $this->Beratbadan_m->bbideal($t,$b);
        $data = array(
            'berat' => $b,
            'hasil' => $this->Beratbadan_m->bbideal($t,$b)
        );
		$this->load->view('templates/Main',$data);
	}
}