<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelas extends MY_Controller {

    public function __construct(){
        parent ::__construct();
        $this->load->helper('form');
        $this->load->model('Kelas_m');
        $this->load->model('Jurusan_m');
        if (!$this->session->login){
            redirect('Login');
        }
    }

    public function index(){
        // $this->load->library('pagination');
        // $data['judul'] = 'Kelas';
        // $this->load->library('pagination');
		// $limit = !empty($this->input->get("limit")) ? $this->input->get("limit") : 5;
		// $limit = $limit == "all" ? 0 : $limit;
		// $search = !empty($this->input->get('search')) ? $this->input->get("search") : "";
		// $sorting = !empty($this->input->get('sort')) ? $this->input->get('sort') : "";
        
        // $data['data'] = $this->Kelas_m->getkelas("", 0, 0, $search)->result_array();
        
        // //membuat pagination  
		// $config['base_url'] = site_url('kelas').'?sort='.$sorting.'&limit='.$limit.'&search='.$search;
		// $config['total_rows'] = count($data['data']);
		// $config['per_page'] = $limit;
		// $config['full_tag_open'] = '<p>';
		// $config['full_tag_close'] = '</p>';
		// $config['num_tag_open'] = "<span style='padding:5px'>";
		// $config['num_tag_close'] = "</span>";
		// $config['page_query_string'] = true;
		

		// $this->pagination->initialize($config);
        // $num_link = $this->input->get('per_page');
        // $data['num_link'] = $num_link;
        
        // if(!empty($sorting)){
        //     $sorting = urldecode($sorting);
        //     $sorting = explode(",", $sorting);
        //     $this->db->order_by($sorting[0], $sorting[1]);
		// }
        
        // //$data['limit_options'] = array(5=>5, 7=>7, 'all'=>"Semua");
        // $data['limit'] = $limit;
        // $data['search'] = $search;
        // $data['sorting'] = $sorting;
        // $data['data'] =  $this->Kelas_m->getkelas("", $limit, $num_link, $search)->result_array();
		// $data['pagination'] = $this->pagination->create_links();
        $this->template('kelas/kelas_v');
    }

    public function data_kelas(){
        $get = $this->input->get();
		
		$limit = !empty($get['limit']) ? $get['limit'] : 0;
		$search = !empty($get['search']) ? $get['search'] : "";
		$offset = !empty($get['offset']) ? $get['offset'] : 0;
		$sort = !empty($get['sort']) ? $get['sort'] : "kode_jurusan";
		$order = !empty($get['order']) ? $get['order'] : "asc";
		
		$data['total'] = $this->Kelas_m->getkelas("", 0, 0, $search)->num_rows();
		
		$this->db->order_by($sort, $order);
		$data['rows'] = $this->Kelas_m->getkelas("",$limit,$offset,$search)->result_array();

		echo json_encode($data);
    }

    public function form(){
        $data['judul'] = 'Tambah Kelas';
        $data['jurusan'] = $this->Jurusan_m->getall()->result();
        $this->template('kelas/tambah_kelas_v',$data);
    }

    public function tambah_kelas(){
        $this->form_validation->set_rules('kode_kls','Kode Kelas','required');
        $this->form_validation->set_rules('nama_kls','Nama Kelas','required');
        $this->form_validation->set_rules('jurusan','Jurusan','required');
        $data['judul'] = 'Tambah Kelas';
        $post = $this->input->post();

        if($this->form_validation->run()==true)
        {
            $data = [
                'kode_kelas' => $post["kode_kls"],
                'nama_kelas' => $post["nama_kls"],
                'id_jurusan' => $post["jurusan"],
                'created_datetime' => date('Y-m-d H:i:s')
            ];

            //$this->Kelas_m->insertkelas($data);
            //redirect(site_url('Kelas'));
            $insert = $this->Kelas_m->insertkelas($data);
            
            if($insert){

	            $this->session->set_flashdata('success_submit', 'Berhasil menambah kelas');
	            redirect(site_url('Kelas'));

        	}else{
        		$this->session->set_flashdata('error_submit', 'Gagal menambah kelas');
        		$this->template('kelas/tambah_kelas_v');
        	}
        }
        else{
            $this->form();
        }      
    }
    
    public function updateform($id){
        $data['jurusan'] = $this->Jurusan_m->getall()->result();
        $data['data'] = $this->Kelas_m->getkelas($id)->row_array();
        $data['judul'] = 'Update Kelas';
		$this->template('kelas/edit_kelas_v', $data);
    }

    public function update_kelas(){
        $this->form_validation->set_rules('kode_kls','Kode Kelas','required');
        $this->form_validation->set_rules('nama_kls','Nama Kelas','required');
        $data['judul'] = 'Update Kelas';
        $post = $this->input->post();
        $id = $post['id'];

        if($this->form_validation->run()==true)
        {
            $data = [
                'kode_kelas' => $post["kode_kls"],
                'nama_kelas' => $post["nama_kls"],
                'id_jurusan' => $post["jurusan"],
                'updated_datetime' => date('Y-m-d H:i:s')
            ];
            $update = $this->Kelas_m->updatekelas($id,$data);

            if($update){

	            $this->session->set_flashdata('success_submit', 'Berhasil mengubah Kelas');
	            redirect(site_url('Kelas'));

        	}else{
        		$this->session->set_flashdata('error_submit', 'Gagal mengubah kelas');
        		
        		$this->updateform($id);
        	}
        }else{
            $this->updateform($id);
        }        
    }

    public function delete_kelas($id){
        //$this->Kelas_m->deletekelas($id);
        
        $delete = $this->Kelas_m->deletekelas($id);
        if($delete){

            $this->session->set_flashdata('success_submit', 'Berhasil menghapus Kelas');
            

    	}else{
    		$this->session->set_flashdata('error_submit', 'Gagal menghapus kelas');
    		
        }
        redirect('Kelas/index');
        
    }

    public function detail_kelas($id){
        $data['judul'] = 'Detail Kelas';
        $data['kelas'] = $this->Kelas_m->detailkelas($id)->row_array();
        $this->template('kelas/detail_kelas_v',$data);   
    }
}