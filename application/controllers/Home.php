<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller{
    // public function __construct(){
    //     parent::__construct();
    //     if (!$this->session->login){
    //         redirect('Login');
    //     }
    // }

    public function index(){
        $data['judul'] = 'Home';
        $data['username'] = $this->session->nama;
        $data['posisi'] = $this->session->posisi;
        $data['sesi'] = $this->session->sesi;
        $data['total'] = $this->db->get('log_login')->num_rows();
        $this->load->view('templates/header',$data);
        $this->load->view('templates/navbar');
        $this->load->view('templates/Home',$data);
        $this->load->view('templates/footer');
    }

    public function logout(){
        $this->session->sess_destroy();
        redirect('Home');
    }
}


?>