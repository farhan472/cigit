<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jurusan extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('form');
		$this->load->model('Jurusan_m');
	}

	public function index($num_link=0)
	{
		$this->load->library('pagination');
		$per_page = 5;
		$search = !empty($this->input->get("search")) ? $this->input->get("search") : "";
		$sorting = $this->input->get('sort');
		$data['data'] = $this->Jurusan_m->getJurusan("", 0, 0, $search)->result_array();
		// echo $this->db->last_query();
		$config['base_url'] = site_url('jurusan'.'?sort='.$sorting);
		$config['total_rows'] = count($data['data']);
		$config['per_page'] = $per_page;
		$config['full_tag_open'] = '<p>';
		$config['full_tag_close'] = '</p>';
		$config['num_tag_open'] = "<span style='padding:5px'>";
		$config['num_tag_close'] = "</span>";
		// $config['page_query_string'] = true;
		

		$this->pagination->initialize($config);
		$data['num_link'] = $num_link;//$this->input->get('per_page');
		if(!empty($sorting)){
			$sorting = explode(",", $sorting);
			// $sorting[0] = "kode_jurusan"
			// $sorting[1] = "asc"
			// $this->db->order_by('title', 'desc');
			$this->db->order_by($sorting[0], $sorting[1]);
		}
		$data['data'] =  $this->Jurusan_m->getJurusan("", $per_page, $num_link, $search)->result_array();
		$data['pagination'] = $this->pagination->create_links();
		
		$this->template('jurusan/jurusan_v', $data);
	}

	public function tambah(){
		$this->template('jurusan/tambah_jurusan_v');
	}

	public function submit_tambah(){

        $this->load->library('form_validation');

        $this->form_validation->set_rules('kode_jurusan', 'Kode Jurusan', 'required');
		$this->form_validation->set_rules('nama_jurusan', 'Nama Jurusan', 'required');

        $this->db->trans_begin();
		$post = $this->input->post();

        if ($this->form_validation->run() == FALSE)
        {
            $this->template('jurusan/tambah_jurusan_v');
        }
        else
        {

        	$data = ["kode_jurusan" => $post["kode_jurusan"], "nama_jurusan" => $post["nama_jurusan"], "created_datetime" => date('Y-m-d H:i:s') ];
        	$insert = $this->Jurusan_m->insertJurusan($data);

        	$data_log = array(
        		"action" => "insert",
        		"data" => json_encode($data),
        		"table" => "Jurusan",
        		"table_id" => $insert,
        		"created_datetime" => date('Y-m-d H:i:s')
        	);

        	$this->Jurusan_m->insertLog($data_log);

        	if($this->db->trans_status() === FALSE){

				$this->db->trans_rollback();
				$this->session->set_flashdata('error_submit', 'Gagal menambah jurusan');
        		$this->template('jurusan/tambah_jurusan_v');

			}else{

				$this->db->trans_commit();
				$this->session->set_flashdata('success_submit', 'Berhasil menambah jurusan');
	            redirect(site_url('Jurusan'));

			}

        }
	}

	public function edit($id){

		$data['data'] = $this->Jurusan_m->getJurusan($id)->row_array();
		$this->template('jurusan/edit_jurusan_v', $data);

	}

	public function submit_edit(){

        $this->load->library('form_validation');

        $this->form_validation->set_rules('kode_jurusan', 'Kode Jurusan', 'required');
		$this->form_validation->set_rules('nama_jurusan', 'Nama Jurusan', 'required');

		$post = $this->input->post();
		$id = $post['id'];

        if ($this->form_validation->run() == FALSE)
        {
            // $this->template('jurusan/edit_jurusan_v');
            $this->edit($id);
        }
        else
        {
        	$data = ["kode_jurusan" => $post["kode_jurusan"], "nama_jurusan" => $post["nama_jurusan"]];
        	
        	$update = $this->Jurusan_m->updateJurusan($id, $data);

        	if($update){

	            $this->session->set_flashdata('success_submit', 'Berhasil mengubah jurusan');
	            redirect(site_url('Jurusan'));

        	}else{
        		$this->session->set_flashdata('error_submit', 'Gagal mengubah jurusan');
        		// $this->template('jurusan/edit_jurusan_v');
        		$this->edit($id);
        	}

        }
	}

	public function hapus($id){

		$delete = $this->Jurusan_m->deleteJurusan($id);

    	if($delete){

            $this->session->set_flashdata('success_submit', 'Berhasil menghapus jurusan');
            

    	}else{
    		$this->session->set_flashdata('error_submit', 'Gagal menghapus jurusan');
    		
    	}
    	redirect(site_url('Jurusan'));
	}



	private function pagination_config(){
		$config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['next_link']        = 'Next';
        $config['prev_link']        = 'Prev';
        $config['full_tag_open']    = '<div class="pagging"><nav><ul class="pagination">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close']  = '</span>Next</li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';

        return $config;
	}

}

/* End of file Tambah_kelas.php */
/* Location: ./application/controllers/Tambah_kelas.php */