<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ControllerMath extends CI_Controller {


	function __construct() {
		parent::__construct();
		$this->load->model('Math_m');
		if (!$this->session->login){
			redirect('Login');
		}
	}

	public function index(){
		$data['judul'] = 'Perhitungan Luas' ;
		$data['info'] = '';
		$this->load->view('templates/header',$data);
		$this->load->view('templates/Navbar');
		$this->load->view('form/menghitung_luas',$data);
		$this->load->view('templates/Footer');
	}


	public function persegipanjang(){
	$this->form_validation->set_rules('panjang','Panjang','required|numeric');
	$this->form_validation->set_rules('lebar','Lebar','required|numeric');
	$data['info'] = '';

	if (isset($_POST['submit']) && $this->form_validation->run()==TRUE)
	{
	$panjang = $this->input->post('panjang');
	$lebar = $this->input->post('lebar');
	$luas = $this->Math_m->LuasPersegiPanjang($panjang,$lebar);
	$data['info'] = "	<p>Panjang : $panjang</p>
						<p>lebar   : $lebar</p>
						<br>
						<h4>Luas Persegi Panjang : $luas</h4>";
	$this->load->view('templates/header',$data);
	$this->load->view('templates/navbar');
	$this->load->view('form/Menghitung_luas',$data);
	$this->load->view('templates/footer');
	}
		
	else {
		$data['judul'] = 'Perhitungan Luas' ;
		$this->load->view('templates/header',$data);
		$this->load->view('templates/navbar');
		$this->load->view('form/Menghitung_luas');
		$this->load->view('templates/footer');
		}
	}
}
