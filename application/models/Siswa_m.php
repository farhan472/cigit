<?php 
class Siswa_m extends CI_Model {
   public function __construct() {
      parent::__construct();
   }

   public function get() {
      // $query = $this->db->query('select * from siswa');
      // if($query) {
      //    return $query->result();
      // } else {
      //    return false;
      // }
      return $this->db->get('siswa')->result_array(); 
   }

   public function create($data) {
      // $query = $this->db->query("insert into siswa (nama,alamat,email,tanggal_lahir,agama,jenis_kelamin,no_telp,tinggi,berat) 
      // values ('farhan','ciracas','arfianto@gmail.com','2002-07-04','islam','laki-laki','08123456789','165','55')");
      // if($query) {
      //    return true;
      // } else {
      //    return false;
      // }
      
      //$foto = $_FILES['file']['name'];
      
      // $data = [
      //    "nama" => $this->input->post('nama'),
      //    "email" => $this->input->post('email'),
      //    "no_telp" => $this->input->post('notelp'),
      //    "tanggal_lahir" => $this->input->post('tlahir'),
      //    "jenis_kelamin" => $this->input->post('jenis_kelamin'),
      //    "alamat" => $this->input->post('alamat'),
      //    "agama" => $this->input->post('agama'),
      //    "tinggi" => $this->input->post('tinggi'),
      //    "berat" => $this->input->post('berat'),
      //    "foto" => $_FILES[f'ile']['name']
      // ];

      $this->db->insert('siswa',$data);
   }

   public function getsiswa($id="",$limit=0,$offset=0,$search=""){
      if(!empty($id)){
          $this->db->where('kelas.id',$id);
      }
      $this->db->select('siswa.*');
      if(!empty($search)){
        $this->db->group_start();
        $this->db->like('LOWER(kode_kelas)',strtolower($search));
        $this->db->or_like('LOWER(nama_kelas)',strtolower($search));
        $this->db->or_like('LOWER(nama_jurusan)',strtolower($search));
        $this->db->group_end();
     }
     return $this->db->get('siswa', $limit, $offset);
  }

   public function updatedatasiswa($id,$data) {
      
      //menangkap data sebaiknya di lakukan di controller 
      // $data = [
      //    "nama" => $this->input->post('nama'),
      //    "email" => $this->input->post('email'),
      //    "no_telp" => $this->input->post('notelp'),
      //    "tanggal_lahir" => $this->input->post('tlahir'),
      //    "jenis_kelamin" => $this->input->post('jenis_kelamin'),
      //    "alamat" => $this->input->post('alamat'),
      //    "agama" => $this->input->post('agama'),
      //    "tinggi" => $this->input->post('tinggi'),
      //    "berat" => $this->input->post('berat'),
      //    "foto" => $_FILES['file']['name']
      // ];

      $this->db->where('id',$id);
      $this->db->update('siswa',$data);
   }

   public function detail($id){
     return $this->db->get_where('siswa',['id' => $id])->row_array();

   //   $this->db->query('SELECT * FROM ' . $this->siswa . ' WHERE id='.$id);
   }

   // public function update($id) {
   //    $query = $this->db->query("update siswa set alamat='bogor' where id =".$id);
   //    if($query) {
   //       return true;
   //    } else {
   //       return false;
   //    }
   // }


   public function delete($id) {
      // $query = $this->db->query("delete from siswa where id =".$id);
      // if($query) {
      //    return true;
      // } else {
      //    return false;
      // }
      $this->db->where('id',$id);
      $this->db->delete('siswa');
   }
}