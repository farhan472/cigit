<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelas_m extends CI_Model {
    public function insertkelas($data){
        return $this->db->insert('kelas',$data);
    }
    
    public function getkelas($id="",$limit=0,$offset=0,$search=""){
       if(!empty($id)){
           $this->db->where('kelas.id',$id);
       }
       $this->db->select('kelas.*,jurusan.nama_jurusan');
       $this->db->join('jurusan','kelas.id_jurusan = jurusan.id','left');
    	if(!empty($search)){
			$this->db->group_start();
			$this->db->like('LOWER(kode_kelas)',strtolower($search));
			$this->db->or_like('LOWER(nama_kelas)',strtolower($search));
			$this->db->or_like('LOWER(nama_jurusan)',strtolower($search));
			$this->db->group_end();
		}
		return $this->db->order_by('updated_datetime', 'asc')->get('kelas', $limit, $offset);
	}
    
    public function updatekelas($id,$data){
        $this->db->where('id',$id);
        return $this->db->update('kelas',$data);
    }
    
    public function deletekelas($id){
        $this->db->where('id',$id);
        return $this->db->delete('kelas');
    }

    // public function sortby($sort){
		
	// 	if($sort == "KKasc"){
	// 		$this->db->order_by('kode_kelas','ASC');
	// 	}else if ($sort == "KKdesc") {
	// 		$this->db->order_by('kode_kelas','DESC');
	// 	}else if ($sort == "NKasc") {
	// 		$this->db->order_by('nama_kelas','ASC');
	// 	}else if ($sort == 'NKasc') {
	// 		$this->db->order_by('nama_kelas','DESC');
    //     }
        
    //     $this->db->select('kelas.*,jurusan.nama_jurusan');
    //     $this->db->from('kelas');
    //     $this->db->join('jurusan','kelas.id_jurusan = jurusan.id');
        
	// 	return $this->db->get();
	// }
    
    public function detailkelas($id){
        return $this->db->get_where('kelas',['id' => $id]);
    }

   




}