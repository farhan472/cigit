<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Beratbadan_m extends CI_Model {
    public function __construct() {
		parent::__construct();
    }
    
    public function bbideal($tinggi,$berat) {
        $tinggi = $tinggi/100; //ngubah cm ke m
        $bmi = $berat/($tinggi*$tinggi);   //rumus
        //klasifikasi hasil dari rumus 
        if($bmi > 27)
			return "Obesitas";
		else if($bmi >= 25)
			return "Overweight";
		else if($bmi >= 18)
			return "Normal";
		else
			return "Underweight";        
	}
}