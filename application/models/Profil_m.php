<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profil_m extends CI_Model {

	public function getuser($id){
		return $this->db->get_where('user',['id' => $id]);
	}

	public function edituser($id, $data){
		$this->db->where('id', $id);
		return $this->db->update('user', $data);
		
    }
}