<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Upload_m extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    // public function _uploadImage($foto)
    // {   
    //     $config['upload_path']          = './upload/product/';
    //     $config['allowed_types']        = 'gif|jpg|png';
    //     $config['encrypt_name']         = TRUE ;
    //     $config['max_size']             = 1024; // 1MB
    //     // $config['max_width']            = 1024;
    //     // $config['max_height']           = 768;
    //     $upload = $this->upload($foto);

    //     if ($this->upload->do_upload($upload)){
    //         print_r($this->upload->data());
    //     } else {
    //         print_r($this->upload->display_errors());
    //     }
        
    // }

    public function bbideal($tinggi,$berat) {
        $tinggi = $tinggi/100; //ngubah cm ke m
        $bmi = $berat/($tinggi*$tinggi);   //rumus
        //klasifikasi hasil dari rumus 
        if($bmi > 27)
			return "Obesitas";
		else if($bmi >= 25)
			return "Overweight";
		else if($bmi >= 18)
			return "Normal";
		else
			return "Underweight";        
	}
}
