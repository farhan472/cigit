<?php 
class Karyawan_m extends CI_Model {
   public function __construct() {
      parent::__construct();
   }

   public function getall() {
      return $this->db->get('karyawan'); 
   }

   public function create($data) {
      $this->db->insert('karyawan',$data);
   }

   public function updatekaryawan($id,$data) {
      $this->db->where('id',$id);
      $this->db->update('karyawan',$data);
   }

   public function detail($id){
     return $this->db->get_where('karyawan',['id' => $id])->row_array();
   }


   public function delete($id) {
      $this->db->where('id',$id);
      $this->db->delete('karyawan');
   }

   public function getemail(){
      return $this->db->select('karyawan.email');
   }
}