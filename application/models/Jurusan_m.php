<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jurusan_m extends CI_Model {

	public function insertJurusan($data){
		$this->db->insert('jurusan', $data);

		// return ["insert_id" => $this->db->insert_id(), "affected_rows"=>$this->db->affected_rows()];
		return $this->db->affected_rows();
	}

	public function getJurusan($id="", $limit=0, $offset=0,$search=""){
		if(!empty($id)){
			$this->db->where('id', $id);
		}
		if(!empty($search)){
			$this->db->group_start();
			$this->db->like('LOWER(kode_jurusan)',strtolower($search));
			$this->db->or_like('LOWER(nama_jurusan)',strtolower($search));
			$this->db->group_end();
		}
		// 
		// $this->db->get('table_name', $limit, $offset);
		return $this->db->order_by('updated_datetime', 'asc')->get('jurusan', $limit, $offset);
	}

	public function updateJurusan($id, $data){
		$this->db->where('id', $id);
		return $this->db->update('jurusan', $data);
		
	}

	public function deleteJurusan($id){
		$this->db->where('id', $id);
		$this->db->delete('jurusan');

		return $this->db->affected_rows();
	}
	
	public function getall(){
        return $this->db->get('jurusan');
	}
	
	
	// public function sortby($sort){
		
	// 	if($sort = "KJasc"){
	// 		$this->db->order_by('kode_jurusan', 'ASC');
	// 	}else if ($sort = "KJdesc") {
	// 		$this->db->order_by('kode_jurusan', 'DESC');
	// 	}else if ($sort = "NJasc") {
	// 		$this->db->order_by('nama_jurusan', 'ASC');
	// 	}else if ($sort = "NJasc") {
	// 		$this->db->order_by('nama_jurusan', 'DESC');
	// 	}
		
	// 	return $this->db->get('jurusan');
	// }

}

/* End of file Jurusan_m.php */
/* Location: ./application/models/Jurusan_m.php */