<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {
	
	protected function template($view, $data=array())
	{
		$this->load->view('templates/header', $data);
		$this->load->view('templates/navbar');
		$this->load->view($view, $data);
		$this->load->view('templates/footer', $data);
	}
	
	protected function send_email($penerima= '', $subject ='' ,$pesan = '',$nama=''){
		$this->load->library('email');
		$config = [
			'mailtype' => 'html',
			'charset' => 'utf-8',
			'protocol' => 'smtp',
			'smtp_host' => 'ssl://smtp.gmail.com',
			'smtp_user' => 'arfianto472@gmail.com',
			// 'smtp_pass' => 'ctbqtkzrdvqtrfmb',
			'smtp_pass' => 'bzlxrgnidjlrimdc',
			'smtp_port' => '465',
			'wordwrap' => TRUE,
			'crlf' => "\r\n",
			'newline' => "\r\n"
		];

		$this->email->initialize($config);

		$this->load->library('parser');
		$data['message'] = $pesan;
		$data['judul'] = $subject;
		$data['head'] = $subject;
		$data['nama'] = $nama;
		// $data['gambar'] = base_url('assets/images/Email.png');
		$data['gambar'] = 'https://raw.githubusercontent.com/ColorlibHQ/email-templates/master/10/images/email.png';

		// $this->output->set_header('content-type: text/css');
		// $this->email->set_header('Content-Type', 'text/html');

		$body = $this->parser->parse("Email/emailtemplate2",$data,true);

		$this->email->from('arfianto472@gmail.com','No-reply');
		$this->email->to($penerima);
		$this->email->subject($subject);
		$this->email->message($body);

		return $this->email->send();
	}

}

/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */