<div class="container">
<h2>Daftar Jurusan</h2>
<p><?php echo $this->session->flashdata('success_submit'); ?></p>
<br/>						
	<a href="<?= site_url('Jurusan/tambah');?>" class="btn btn-success">Tambah Data Jurusan</a><br>
<br>
<div id="toolbar">
  <button id="button" class="btn btn-secondary">getSelections</button>
</div>
<!-- <table class="table" 
	id="tablejurusan"
	data-toolbar="#toolbar"
	data-toggle="table" 
	data-search="true" 
	data-show-refresh="true"
	data-pagination="true"
	data-page-size="5"
	data-page-list="[5, All]"
	data-show-columns="true"
	data-show-toggle="true"
	data-side-pagination="server"
	data-detail-view="true"
	data-detail-formatter="detailFormatter"
	data-resizable="false"
	data-show-export="true"
	data-export-types="['pdf', 'csv', 'excel']"
	data-export-options='{"fileName": "data_jurusan"}'
	data-click-to-select="true"
	data-url="<?php //echo site_url('Jurusan/data_jurusan')?>"
	>
	<thead>
		<tr>
			<th	data-field="state" data-checkbox="true"></th>
			<th data-formatter="numberFormatter">No</th>
			<th data-field="kode_jurusan" data-sortable="true">Kode Jurusan</th>
			<th data-field="nama_jurusan" data-sortable="true">Nama Jurusan</th>
			<th data-formatter="actionFormatter" data-field="id" data-force-hide="true">Aksi</th>
		</tr>
	</thead>
</table> -->
	<table id="tablejurusan"></table>
</div>

<script type="text/javascript" src="<?= base_url("assets/js/configBsTable.js")?>"></script>
<script type=text/javascript>
$(document).ready(function() {
		const $daftar_jurusan = $('#tablejurusan');

		$daftar_jurusan.bootstrapTable({
			...config,
			detailFormatter:detailFormatter,
			url:"<?= site_url("jurusan/data_jurusan") ?>",
			exportOptions:{fileName: "Data Jurusan"},
			columns: [ 
				{
					checkbox : true,
				},
				{
					title:"No",
					formatter:numberFormatter,
				}, 
				{
					title: 'Kode Jurusan',
					field: 'kode_jurusan',
					sortable:true,
				},
				{
					title: 'Nama Jurusan',
					field: 'nama_jurusan',
					sortable:true,
				},
				{
					title:"Aksi",
					field:"id",
					sortable:false,
					searchable:false,
					formatter:actionFormatter,
				},
			]
		})
	})



	function numberFormatter(value, row, index) {
	var options = $('#tablejurusan').bootstrapTable('getOptions')
	// alert(options["pageNumber"] + " " + options["pageSize"])
	//  console.log(options["pageSize"]);
	var tes = 0
	if (!isNaN(options['pageSize'])) { //cek pagesize angka atau tidak, klo angka jalankan kode dibawah
	tes = ((options["pageNumber"] - 1) * options["pageSize"])
	}
	return index + 1 + tes;
	}

	function actionFormatter(value, row, index){
		return[
			'<a href="<?= site_url("Jurusan/edit/") ?>'+value+'" class="btn btn-success ">Edit</a>',
			' ',
			'<a href="<?= site_url("Jurusan/hapus/") ?>'+value+'" class="btn btn-danger"  >Hapus</a>',
		].join('');
	}

	function detailFormatter(index, row){
		var html = []
		const excludeType = ["Undefined","object"];
		$.each(row, function (key, value) {
			if(!excludeType.includes(typeof value)){
				html.push('<p><b>' + key + ':</b>' + value + '</p>')
			}
		})
		return html.join('')
	}

	$(document).ready(function(){
	var table = $('#tablejurusan')
	var $button = $('#button')
		
		$button.click(function () {
		alert('Jurusannya : ' + JSON.stringify(table.bootstrapTable('getSelections')[0]["nama_jurusan"]))
		})
	})
</script>