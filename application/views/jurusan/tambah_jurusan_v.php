<div class="container">
	<h2>Tambah Jurusan</h2>
	<?php echo $this->session->flashdata('error_submit'); ?>
	<form action="<?php echo site_url('jurusan/submit_tambah') ?>" method="post">
		<table class="table table-responsive">
			<tr>
				<td>Kode Jurusan:</td>
				<td>
					<input type="text" class="form-control" name="kode_jurusan" value="<?php echo set_value("kode_jurusan") ? set_value("kode_jurusan") : ''  ?>">
					<?php echo form_error('kode_jurusan', "<span>", "</span>"); ?>
				</td>
			</tr>
			<tr>
				<td>Nama Jurusan:</td>
				<td>
					<input type="text" class="form-control" name="nama_jurusan" value="<?php echo set_value("nama_jurusan") ? set_value("nama_jurusan") : "" ?>">
					<?php echo form_error('nama_jurusan', "<span>", "</span>"); ?>
					
				</td>
			</tr>
			<tr></tr>
			<tr>
		</table>
				<button type="submit" class="btn btn-primary">Submit</button>
				<a href="#" class="btn btn-primary" onclick="history.go(-1)">Kembali</a>
	</form>
</div>