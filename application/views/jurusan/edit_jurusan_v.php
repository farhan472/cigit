<div class="container">
	<h2>Edit Jurusan</h2>
	<?php echo $this->session->flashdata('error_submit'); ?>
	<form action="<?php echo site_url('jurusan/submit_edit') ?>" method="post">
		<table class="table table-responsive">
			<tr>
				<td>Kode Jurusan:</td>
				<td>
					<?php $curval =  set_value("id") ? set_value("id") : $data['id']?>
					<input type="hidden" class="form-control" name="id" value="<?php echo $curval ?>">
					<?php $curval =  set_value("kode_jurusan") ? set_value("kode_jurusan") : $data['kode_jurusan']?>
					<input type="text" name="kode_jurusan" class="form-control" value="<?php echo $curval  ?>">
					<?php echo form_error('kode_jurusan', "<span>", "</span>"); ?>
				</td>
			</tr>
			<tr>
				<td>Nama Jurusan:</td>
				<td>
					<?php $curval =  set_value("nama_jurusan") ? set_value("nama_jurusan") : $data['nama_jurusan']?>
					<input type="text" name="nama_jurusan" class="form-control" value="<?php echo $curval ?>">
					<?php echo form_error('nama_jurusan', "<span>", "</span>"); ?>
					
				</td>
			</tr>
		</table>
				<button type="submit" class="btn btn-primary">Submit</button>
				<a href="#" onclick="history.go(-1)" class="btn btn-primary">Kembali</a>
	</form>
</div>