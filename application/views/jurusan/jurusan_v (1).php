
<style>
  table,
  th,
  td {
    padding: 10px;
    border: 1px solid black;
    border-collapse: collapse;
  }
</style>


<h2>Daftar Jurusan</h2>
<p><?php echo $this->session->flashdata('success_submit'); ?></p>
<br/>
<table>
	<tr>
		<td>No</td>
		<td>Kode Jurusan</td>
		<td>Nama Jurusan</td>
		<td>Aksi</td>
	</tr>
	<?php
	$no = $num_link+1;
	foreach ($data as $key => $value) { ?>
		<tr>
			<td><?php echo $no ?></td>
			<td><?php echo $value['kode_jurusan'] ?></td>
			<td><?php echo $value['nama_jurusan'] ?></td>
			<td>
				<a href="<?php echo site_url("jurusan/edit/".$value['id']) ?>">Edit</a>
				<a href="<?php echo site_url("jurusan/hapus/".$value['id']) ?>">Hapus</a>
			</td>
		</tr>
	<?php $no++; } ?>
</table>
<?php echo $pagination ?>
