<section>
    <div class="container">
		<h2>Daftar Kelas</h2>
        <p><?php echo $this->session->flashdata('success_submit'); ?></p>
        <br/>
        	<a href="<?= site_url('Kelas/form');?>" class="btn btn-success">Tambah Data Kelas</a>
		<br><br>
		<div id="toolbar">
			<button id="button" class="btn btn-secondary">getSelections</button>
		</div>
		<table class="table" 
		id="tablekelas"
		data-toggle="table" 
		data-search="true" 
		data-show-refresh="true"
		data-pagination="true"
		data-page-size="10"
		data-page-list="[5,10, All]"
		data-show-columns="true"
		data-show-toggle="true"
		data-side-pagination="server"
		data-show-pagination-switch ="true"
		data-detail-view="true"
		data-detail-formatter="detailFormatter"
		data-resizable="false"
		data-show-export="true"
		data-export-types="['pdf', 'csv', 'excel']"
		data-export-options='{"fileName": "data_kelas"}'
		data-click-to-select="true"
		data-url="<?php echo site_url('Kelas/data_kelas')?>">
            <thead>
            <tr>
				<th	data-field="radio" data-radio="true"></th>
                <th data-formatter="numberFormatter">No</th>
                <th data-field="kode_kelas" data-sortable="true">Kode Kelas</th>
                <th data-field="nama_kelas" data-sortable="true">Nama Kelas</th>
                <th data-field="nama_jurusan" data-sortable="true">Nama_Jurusan</th>
                <th data-formatter="actionFormatter" data-field="id" data-force-hide="true">Aksi</th>
            </tr>
			</thead>
        </table>
    </div>
</section>
<script>
	
	function numberFormatter(value, row, index) {
	var options = $('#tablekelas').bootstrapTable('getOptions')
	// alert(options["pageNumber"] + " " + options["pageSize"])
	//  console.log(options["pageSize"]);
	var tes = 0
	if (!isNaN(options['pageSize'])) { //cek pagesize angka atau tidak, klo angka jalankan kode dibawah
	tes = ((options["pageNumber"] - 1) * options["pageSize"])
	}
	return index + 1 + tes;
	}

	function actionFormatter(value, row, index){
		return[
			'<a href="<?= site_url("Kelas/detail_kelas/") ?>'+value+'" class="btn btn-primary ">Detail</a>',
			' ',
			'<a href="<?= site_url("Kelas/updateform/") ?>'+value+'" class="btn btn-success ">Edit</a>',
			' ',
			'<a href="<?= site_url("Kelas/delete_kelas/") ?>'+value+'" class="btn btn-danger"  >Hapus</a>',
		].join('');
	}

	function detailFormatter(index, row){
		var html = []
		const excludeType = ["Undefined","object"];
		$.each(row, function (key, value) {
			if(!excludeType.includes(typeof value)){
				html.push('<p><b>' + key + ':</b>' + value + '</p>')
			}
		})
		return html.join('')
	}

	$(document).ready(function(){
	var table = $('#tablekelas')
	var $button = $('#button')
		
		$button.click(function () {
		alert('Kelasnnya :  ' + JSON.stringify(table.bootstrapTable('getSelections')[0]["nama_kelas"]))
		})
	})

</script>