<section class="container">
    <table class="table table-responsive">
        <tr>
            <th>ID</th>
            <th>Kode Kelas</th>
            <th>Nama Kelas</th>
            <th>ID Jurusan</th>
            <th>Created date</th>
            <th>Update date</th>
        </tr>
        <tr>
            <td><?= $kelas['id']?></td>
            <td><?= $kelas['kode_kelas']?></td>
            <td><?= $kelas['nama_kelas']?></td>
            <td><?= $kelas['id_jurusan']?></td>
            <td><?= $kelas['created_datetime']?></td>
            <td><?= $kelas['updated_datetime']?></td>
        </tr>
    </table>
    <a href="<?= base_url();?>Kelas/updateform/<?= $kelas['id']?>" class="btn btn-success">Edit</a>
    <a href="#" onclick="history.go(-1)" class="btn btn-primary">Kembali</a>
</section>