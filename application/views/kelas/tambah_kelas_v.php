<section>
    <div class="container">
        <?php echo $this->session->flashdata('error_submit'); ?>
        <form method="POST" action="<?php echo site_url('kelas/tambah_kelas');?>">
            <div class="form_group">
                <label for="text">Kode Kelas</label>
                <input type="text" name="kode_kls" class="form-control" value="">
                <small><?php echo form_error('kode_kls');?></small>
            </div>
            <br>
            <div class="form_group">
                <label for="text">Nama Kelas</label>
                <input type="text" name="nama_kls" class="form-control" value="">
                <small><?php echo form_error('nama_kls');?></small>
            </div>
            <br>
            <div class="form-group">
                <small><?php echo form_error('jurusan');?></small>
                <select name="jurusan" id="jurusan" class="form-control" required>
                    <option>>> Jurusan <<</option>
                    <?php foreach ($jurusan as $key ) : ?>
                    <option value="<?php echo $key->id ?>"><?php echo $key->nama_jurusan?></option>
                    <?php endforeach ?>
                </select>
            </div>   
            <br>
            <button type="submit" class="btn btn-primary" name="tombol" value="simpan" >Submit</button>
            <a href="#" onclick="history.go(-1)" class="btn btn-primary">Kembali</a>
        </form>
    </div>
</section>