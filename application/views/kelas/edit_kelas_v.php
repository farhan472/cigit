<section>
    <div class="container">
        <?php echo $this->session->flashdata('error_submit'); ?>
        <form method="POST" action="<?php echo site_url('kelas/update_kelas');?>">
                <input type="hidden" name="id" value="<?php echo $data['id'];?>">
            <div class="form_group">
                <label for="text">Kode Kelas</label>
                <input type="text" class="form-control" name="kode_kls" value="<?php echo $data['kode_kelas'];?>">
                <small><?php echo form_error('kode_kls');?></small>
            </div>
            <br>
            <div class="form_group">
                <label for="text">Nama Kelas</label>
                <input type="text" class="form-control" name="nama_kls" value="<?php echo $data['nama_kelas'];?>">
                <small><?php echo form_error('nama_kls');?></small>
            </div>
            <br>
            <div class="form-group">
                <select name="jurusan" id="jurusan" class="form-control">
                    <?php foreach ($jurusan as $key ) : ?>
                        <?php if($key->id == $data['id_jurusan']):?>
                            <option value="<?php echo $key->id ?>" selected><?php echo $key->nama_jurusan?></option>
                        <?php else :?>
                            <option value="<?php echo $key->id ?>"><?php echo $key->nama_jurusan?></option>
                        <?php endif; ?>
                    <?php endforeach ?>
                </select>
            </div>
            <br>
            <button type="submit" class="btn btn-primary" name="tombol" value="simpan" >Submit</button>
            <a href="#" onclick="history.go(-1)" class="btn btn-primary">Kembali</a>
        </form>
    </div>
</section>