<div class="container">
    <a href="<?= site_url('Karyawan/daftar');?>" class="btn btn-primary ">Tambah Data Siswa</a>
    <br><br>
    <table class="table table-responsive">
        <tr>
            <th>First Name</th>
            <th>Email</th>
            <th>Nomor Telepon</th>
            <th>Jenis Kelamin</th>           
        </tr>
        <?php foreach($Karyawan as $key) : ?>
        <tr>
            <td><?php echo $key['firstname']?></td>     
            <td><?php echo $this->encryption->decrypt($key['email'])?></td>     
            <td><?php echo $key['no_telp']?></td>       
            <td><?php echo $key['jenis_kelamin']?></td>     
            <!-- <td><a href="<?//= base_url();?>Karyawan/detail/<?//= $key['id']?>" class="btn btn-primary center-block">Detail</a></td>     
            <td><a href="<?//= base_url();?>Karyawan/update/<?//= $key['id']?>" class="btn btn-success center-block">Edit</a></td>     
            <td><a href="<?//= base_url();?>Karyawan/delete/<?//= $key['id']?>/<?//= $key['foto']?>" class="btn btn-danger center-block" onclick="return confirm('Yakin ingin menghapus data?');">Delete</a></td>      -->
            <td><a href="<?php echo site_url('Karyawan/detail/'.str_replace(['/','=','+'],['garing','samadengan','ples'],$this->encryption->encrypt($key['id'])))?>" class="btn btn-primary center-block">Detail</a></td>
            <td><a href="<?php echo site_url('Karyawan/update/'.str_replace(['/','=','+'],['garing','samadengan','ples'],$this->encryption->encrypt($key['id'])))?>" class="btn btn-success center-block">Update</a></td>
            <td><a href="<?php echo site_url('Karyawan/delete/'.str_replace(['/','=','+'],['garing','samadengan','ples'],$this->encryption->encrypt($key['id'])))?>" class="btn btn-danger center-block"  onclick="return confirm('Yakin ingin menghapus data?');">Delete</a></td>
        </tr>
        <?php endforeach;?>
    </table>
</div>