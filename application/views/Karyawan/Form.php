<br>
<section class="container" style="text-align:center;" >  
  <div class="jumbotron">  
    <p><?php echo $this->session->flashdata('error'); ?></p>
    <form method="POST" action="<?php echo site_url('Karyawan/Daftar_success')?>" enctype="multipart/form-data" style="width:40%;margin-left:30%;">
      <div class="form-group">
        <label for="exampleInputEmail1">First Name</label>
        <input type="text" name="firstname" class="form-control" value="<?php if(!empty($firstname)){echo $firstname;}?>" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter FirstName">
        <small class="form-text text-danger"><?= form_error('firstname');?></small>
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Last Name</label>
        <input type="text" name="lastname" class="form-control" value="<?php if(!empty($lastname)){echo $lastname;}?>" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter LastName">
        <small class="form-text text-danger"><?= form_error('lastname');?></small>
      </div>
      <div class="form-group">
        <label>Email</label>
        <input type="email" name="email" class="form-control" placeholder="Enter Email" value="<?php if(!empty($email)){echo $email; }?>">
        <small class="form-text text-danger"><?= form_error('email');?></small>
      </div>
      <div class="form-group">
        <label for="exampleInputPassword1">Tanggal Lahir</label>
        <input type="date" name="tgl_lahir" class="form-control" value="<?php if(!empty($tgl_lahir)){echo $tgl_lahir;}?>" id="exampleInputPassword1">
        <small class="form-text text-danger"><?= form_error('tgl_lahir');?></small>
      </div>
      <div class="form-group">
        <label for="exampleInputPassword1">Alamat</label>
        <input type="alamat" name="alamat" class="form-control" value="<?php if(!empty($alamat)){echo $alamat;}?>" id="exampleInputPassword1" placeholder="Enter Alamat">
        <small class="form-text text-danger"><?= form_error('alamat');?></small>
      </div>
      <div class="form-group">
        <label>Tambahkan Foto Anda</label>
        <input type="file" accept="image/*" name="foto" class="form-control" value="" required>
        <small class="form-text text-danger"><?= form_error('foto');?></small>
      </div>
      <div class="form-group">
        <label>Tambahkan File CV</label>
        <input type="file" accept=".doc,.docx,.pdf" name="file" class="form-control" value="" requried>
        <small class="form-text text-danger"><?= form_error('file');?></small>
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Hobi</label>
        <input type="text" name="hobi" class="form-control" value="<?php if(!empty($hobi)){echo $hobi;}?>" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Hobi">
        <small class="form-text text-danger"><?= form_error('hobi');?></small>
      </div>
      <div class="form-group">
        <label>Deskripsi Diri</label>
        <textarea name="deskripsi" class="form-control"><?php if(!empty($deskripsi)){echo $deskripsi;}?></textarea>
        <small class="form-text text-danger"><?= form_error('deskripsi');?></small>
      </div>
      <div class="form-group">
        <label>Jenis Kelamin</label>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="jenis_kelamin" value="laki-laki" <?php  if(!empty($jenis_kelamin)) echo ($jenis_kelamin=='laki-laki')?'checked':'' ?>> Laki-laki
            <input class="form-check-input" type="radio" name="jenis_kelamin" value="perempuan" <?php  if(!empty($jenis_kelamin)) echo ($jenis_kelamin=='perempuan')?'checked':'' ?>> Perempuan
            <small class="form-text text-danger"><?= form_error('jenis_kelamin');?></small>
          </div>
      </div>
      <div class="form-group">
        <label for="exampleInputPassword1">Nomor Telepon</label>
        <input type="number" name="no_telp" class="form-control" value="<?php if(!empty($no_telp)){echo $no_telp;}?>" id="exampleInputPassword1" placeholder="Enter Nomor Telepon">
        <small class="form-text text-danger"><?= form_error('no_telp');?></small>
      </div>
      <br>
      <button type="submit" name="submit" class="btn btn-primary">Submit</button>
    </form>
    </div>
</section>
  <br>