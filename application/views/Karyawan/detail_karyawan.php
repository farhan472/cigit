<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading text-center">
            <h2 class="panel-title">Detail Data Karyawan</h2>
        </div>
        <div class="panel-body">
            <table class="table table-responsive">
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Tanggal Lahir</th>
                    <th>Alamat</th>
                    <th>Hobi</th>
                    <th>Deskripsi</th>
                    <th>Jenis Kelamin</th>
                    <th>Nomor Telepon</th>
                    <th>Email</th>
                    <th>Foto</th>
                </tr>
                <tr>
                    <td><?php echo $Karyawan['firstname']?></td>     
                    <td><?php echo $Karyawan['lastname']?></td>     
                    <td><?php echo date('d-m-Y',strtotime($Karyawan['tanggal_lahir']))?></td>       
                    <td><?php echo $Karyawan['alamat']?></td>     
                    <td><?php echo $Karyawan['hobi']?></td>     
                    <td><?php echo $Karyawan['deskripsi']?></td>
                    <td><?php echo $Karyawan['jenis_kelamin']?></td>     
                    <td><?php echo $Karyawan['no_telp']?></td>       
                    <td><?php echo $this->encryption->decrypt($Karyawan['email'])?></td>       
                    <td><img src="<?php echo site_url("Karyawan/tampilan/".$Karyawan['foto'])?>" alt="" style="width:150px;height:120px;"></td>     
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><a href="<?= base_url();?>Karyawan/update/<?= $Karyawan['id']?>" class="btn btn-success center-block">Update</a></td>     
                    <td><a href="#" onclick="history.go(-1)" class="btn btn-primary">Kembali</a></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
            <?php //var_dump($Karyawan['foto']);?>
            <?php //echo pg_unescape_bytea($Karyawan['foto']);?>
            <?php //header('Content-type: image/png'); ?>
            <?php //echo base64_decode($Karyawan['foto']);?>
        </div>
    </div>
</div>