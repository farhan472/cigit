<div class="container">		
	<div class="row justify-content-center">
		<div class="col-md-6">	
			<table class="table table-responsive" style="text-align:center;">
				<tr>
					<td>First Name</td>
					<td>:</td>
					<td><?php echo $firstname; ?></td>
				</tr>
				<tr>
					<td>Last Name</td>
					<td>:</td>
					<td><?php echo $lastname; ?></td>
				</tr>
				<tr>
					<td>Alamat</td>
					<td>:</td>
					<td><?php echo $alamat; ?></td>
				</tr>
				<tr>
					<td>Tanggal Lahir</td>
					<td>:</td>
					<td><?php echo $tgl_lahir; ?></td>
				</tr>
				<tr>
					<td>Hobi</td>
					<td>:</td>
					<td><?php echo $hobi; ?></td>
				</tr>
				<tr>
					<td>Deskripsi Diri</td>
					<td>:</td>
					<td><?php echo $deskripsi; ?></td>
				</tr>
				<tr>
					<td>Jenis Kelamin</td>
					<td>:</td>
					<td><?php echo $jenis_kelamin; ?></td>
				</tr>
				<tr>
					<td>Nomor Telepon</td>
					<td>:</td>
					<td><?php echo $no_telp; ?></td>
				</tr>
				<tr>
					<td>Email</td>
					<td>:</td>
					<td><?php echo $email; ?></td>
				</tr>
				<tr>
					<td>Foto</td>
					<td>:</td>
					<td>
					<a href="<?php echo site_url("Karyawan/tampilan/$file_name/$orig_name"); ?>" target='_blank'><?php echo $orig_name?></a>
					<br><img src="<?php echo site_url("Karyawan/tampilan/$file_name/$orig_name"); ?>" style="widht:200px;height:120px;">
					<!-- ada dua cara buat manggil parameter di link "<?php //echo site_url("register/download/$file_name/$orig_name"); ?>" -->
																<!-- "<?php //echo site_url('register/download/'.$file_name.'/'.$orig_name); ?>" -->
					</td>
				</tr> 
				<!-- <tr>
					<td>File</td>
					<td>:</td>
					<td>
					<a href="<?php //echo site_url("Daftar/downloadfile/$file_name/$orig_name"); ?>" target='_blank'><?php //echo $orig_name?></a>
					<!-- <br><img src="<?php //echo site_url("Daftar/downloadfile/$file_name/$orig_name"); ?>" style="widht:200px;height:120px;"> -->
					<!-- ada dua cara buat manggil parameter di link "<?php //echo site_url("register/download/$file_name/$orig_name"); ?>" -->
																<!-- "<?php //echo site_url('register/download/'.$file_name.'/'.$orig_name); ?>" -->
					</td>
				</tr> 
				<tr>	
					<td><a href="<?= site_url('Home');?>" class="btn btn-primary center-block">Home</a></td>
					<td><a href="<?= site_url('Karyawan/index');?>" class="btn btn-primary center-block">Daftar Karyawan</a></td>
				</tr>
			</table>
		</div>
	</div>
</div>