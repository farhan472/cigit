<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
    p{
        text-align:center;
    }
    </style>
</head>
<body>
    <header>
        <?php $this->load->view("templates/header") ?>
    </header>
    <main>
        <p>Berat badan kamu = <?php echo $berat?> </p>
        <p>Hasil olah model = <?php echo $hasil?></p>
    </main>
    <footer>
        <?php $this->load->view("templates/footer") ?>
    </footer>
</body>
</html>