<!DOCTYPE html>
<html>
<head>
	<title>Sistem Akademik Sederhana | SIS ANA</title>

	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css') ?>"  crossorigin="anonymous">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap-grid.min.css') ?>"  crossorigin="anonymous">
	<link rel="stylesheet" href="<?php echo base_url('assets/bootstrap-table/dist/bootstrap-table.min.css') ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/fontawesome-5.14.0/css/fontawesome.min.css') ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/jquery.resizableColumns.css') ?>">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	
  <script src="<?php echo base_url('assets/js/jquery.min.js') ?>" crossorigin="anonymous"></script>
  <script src="<?php echo base_url('assets/js/popper.min.js') ?>" crossorigin="anonymous"></script>
  <script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>" crossorigin="anonymous"></script>

</head>
<body>
