<div class="container">
<!-- <h1> Sederhana</h1> -->
<nav class="navbar navbar-expand-lg navbar-light">
  <a class="navbar-brand" href="<?php echo site_url() ?>"><h1>ADW</h1></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="jurusanDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Sekolah
        </a>
        <div class="dropdown-menu" aria-labelledby="jurusanDropdown">
          <a class="dropdown-item" href="<?php echo site_url('jurusan') ?>">Lihat Jurusan </a>
          <a class="dropdown-item" href="<?php echo site_url('kelas') ?>">Lihat Kelas</a>
          <a class="dropdown-item" href="<?php echo site_url('Siswa/index') ?>">Lihat Siswa</a>
          <a class="dropdown-item" href="<?php echo site_url('Karyawan') ?>">Lihat Karyawan</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url('About') ?>">About</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url('ControllerMath') ?>">Menghitung Luas</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url('Email') ?>">Subscribe</a>
      </li>
    </ul>
    <ul class="navbar-nav navbar-right">
      <?php if($this->session->userdata('nama')) { ?>
        <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url('Home/Logout'); ?>">Logout</a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url('Profil'); ?>">Hey, <?php echo $this->session->userdata('nama');?></a>
        </li>
        <?php } else { ?>
        <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url('Login/index'); ?>">Login</a>
        </li>
        <?php } ?>
    </ul>
  </div>
</nav>
</div>

  <div class="jumbotron">