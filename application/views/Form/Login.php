<br>
<section class="container" style="text-align:center;" id="login" >  
  <div class="jumbotron">
  <p><?php echo $this->session->flashdata('gagallogin'); ?></p>
    <form method="POST" action="<?php echo site_url('login_successfull')?>" style="width:40%;margin-left:30%;">
      <div class="form-group">
        <label for="exampleInputEmail1">Username</label>
        <input type="username" name="username" class="form-control" value="<?php echo $value;?>" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Username">
        <small class="form-text text-danger"><?= form_error('username');?></small>
      </div>
      <div class="form-group">
        <label for="exampleInputPassword1">Password</label>
        <input type="password" name="password" class="form-control" value="<?php if(!empty($password)){echo $password; }?>" id="exampleInputPassword1" placeholder="Password">
        <small class="form-text text-danger"><?= form_error('password');?></small>
      </div>
      <div class="form-check">
        <input name="rememberme" type="checkbox" class="form-check-input" id="exampleCheck1">
        <label  class="form-check-label" for="exampleCheck1">Remember me | </label><a href="<?php echo site_url('Daftar')?>"> Register Here | </a><a href="<?php echo site_url('Home')?>"> Home</a>
      </div>
     <br>
     <br>
      <button type="submit" name="submit" class="btn btn-primary">Submit</button>
    </form>
  </div>
  </section>
  <br>