<section class="container text-center">
    <table class="table table-responsive text-center">
        <tr>
            <td><h2 style="text-align:center;"><b>Login successful!</b></h2></td>
        </tr>
        <tr>
        <td><p class="hello-user">Welcome, <b><?php echo $value ?></b>!</p></td>
        </tr>
        <!-- <tr>
        <td><p class="hello-user" style="text-align:center;">Nama Cookie, <b><?php echo $value ?></b>!</p></td>
        </tr> -->
        <!-- <tr>
        <td><p style="text-align:center;">Kamu Login pukul : <?= $waktu;?></p></td>
        </tr>
        <tr>
        <td><p style="text-align:center;">Expire Login kamu : <?= $expired;?></p></td>
        </tr> -->
    </table>
    <div class="col-md-4 col-md-offset-4">
        <table class="table table-responsive">
            <tr>
                <td><a href="<?= site_url('Register');?>" class="btn btn-primary "> Isi Form</a></td>
                <td><a href="<?= site_url('Login/delete');?>" class="btn btn-primary">Logout</a></td>
                <td><a href="<?= site_url('Siswa/index');?>" class="btn btn-primary">Data Siswa</a></td>
                <td><a href="<?= site_url('Login/check');?>" class="btn btn-primary">Cek Cookie</a></td>
            </tr>
        </table>
    </div>
</section>