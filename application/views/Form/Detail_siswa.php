<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading text-center">
            <h2 class="panel-title">Detail Data Siswa</h2>
        </div>
        <div class="panel-body">
            <table class="table table-responsive">
                <tr>
                    <th>Nama</th>
                    <th>Email</th>
                    <th>Nomor Telepon</th>
                    <th>Tanggal Lahir</th>
                    <th>Jenis Kelamin</th>
                    <th>Agama</th>
                    <th>Alamat</th>
                    <th>Tinggi</th>
                    <th>Berat</th>
                    <th>Foto</th>
                </tr>
                <tr>
                    <td><?php echo $siswa['nama']?></td>     
                    <td><?php echo $siswa['email']?></td>     
                    <td><?php echo $siswa['no_telp']?></td>       
                    <td><?php echo date('d-m-Y',strtotime($siswa['tanggal_lahir']))?></td>       
                    <td><?php echo $siswa['jenis_kelamin']?></td>     
                    <td><?php echo $siswa['agama']?></td>     
                    <td><?php echo $siswa['alamat']?></td>     
                    <td><?php echo $siswa['tinggi']?></td>     
                    <td><?php echo $siswa['berat']?></td>
                    <td><img src="<?php echo site_url("Siswa/tampilan/".$siswa['foto'])?>" alt="" style="width:150px;height:120px;"></td>     
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><a href="<?= base_url();?>Register/update/<?= $siswa['id']?>" class="btn btn-success center-block">Update</a></td>     
                    <td><a href="<?= site_url('Siswa/index');?>" class="btn btn-primary center-block">Kembali</a></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
            <?php //var_dump($siswa['foto']);?>
            <?php //echo pg_unescape_bytea($siswa['foto']);?>
            <?php //header('Content-type: image/png'); ?>
            <?php //echo base64_decode($siswa['foto']);?>
        </div>
    </div>
</div>