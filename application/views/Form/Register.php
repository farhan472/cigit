<div class="container-fluid" style="text-align:center;">
    <div class="row justify-content-center">
        <div class="col-md-6 ">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Form Pendaftaran Siswa</h3>
                </div>
                <div class="panel-body">
                    <form method="POST" action="<?php echo site_url('input'); ?>" enctype="multipart/form-data">
                        <div class="form-group">
                            <label>Nama</label>
                            <input type="text" name="nama" class="form-control" placeholder="Enter Name" value="<?php if(!empty($nama)){echo $nama; }?>">
                            <small class="form-text text-danger"><?= form_error('nama');?></small>
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" name="email" class="form-control" placeholder="Enter Email" value="<?php if(!empty($email)){echo $email; }?>">
                            <small class="form-text text-danger"><?= form_error('email');?></small>
                        </div>
                        <div class="form-group">
                            <label>Nomor Telepon</label>
                            <input type="number" name="notelp" class="form-control" placeholder="Enter No Telepon" value="<?php if(!empty($notelp)){echo $notelp; }?>">
                            <small class="form-text text-danger"><?= form_error('notelp');?></small>
                        </div>
                        <div class="form-group">
                            <label>Tanggal Lahir</label>
                            <input type="date" name="tlahir" class="form-control" value="<?php if(!empty($tlahir)){echo $tlahir; }?>">
                            <small class="form-text text-danger"><?= form_error('tlahir');?></small>
                        </div>
                        <div class="form-group">
                            <label>Jenis Kelamin</label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="jenis_kelamin" value="laki-laki" <?php  if(!empty($jenis_kelamin)) echo ($jenis_kelamin=='laki-laki')?'checked':'' ?>> Laki-laki
                                    <input class="form-check-input" type="radio" name="jenis_kelamin" value="perempuan" <?php  if(!empty($jenis_kelamin)) echo ($jenis_kelamin=='perempuan')?'checked':'' ?>> Perempuan
                                <small class="form-text text-danger"><?= form_error('jenis_kelamin');?></small>
                        </div>
                        <div class="form-group">
                            <label>Agama</label>
                            <select class="form-control" name="agama" value="<?php if(!empty($agama)){echo $agama; }?>">
                                <option value="islam">Islam</option>
                                <option value="kristen">Kristen</option>
                                <option value="katholik">Katholik</option>
                                <option value="hindu">Hindu</option>
                                <option value="budha">Budha</option>
                            </select>
                            <small class="form-text text-danger"><?= form_error('agama');?></small>
                        </div>
                        <div class="form-group">
                            <label>Alamat</label>
                            <textarea name="alamat" class="form-control"><?php if(!empty($alamat)){echo $alamat; }?></textarea>
                            <small class="form-text text-danger"><?= form_error('alamat');?></small>
                        </div>
                        <div class="form-group">
                            <label>Tinggi</label>
                            <input type="number" name="tinggi" class="form-control" placeholder="Tinggi" value="<?php if(!empty($tinggi)){echo $tinggi; }?>">
                            <small class="form-text text-danger"><?= form_error('tinggi');?></small>
                        </div>
                        <div class="form-group">
                            <label>Berat</label>
                            <input type="number" name="berat" class="form-control" placeholder="berat" value="<?php if(!empty($berat)){echo $berat; }?>">
                            <small class="form-text text-danger"><?= form_error('berat');?></small>
                        </div> 
                        <div class="form-group">
                            <label>Tambahkan Foto Anda</label>
                            <input type="file" accept="image/*" name="file" class="form-control" value="<?php if(!empty($foto)){echo $foto; }?>">
                        </div>
                        <button type="submit" class="btn btn-primary" name="tombol" value="Simpan">Save</button>       
                        <br>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>