  <section class="container" style="text-align:center;" id="login" >  
    <form method="POST" action="<?php echo site_url('login_successfull')?>" >
      <div class="form-group">
        <label for="exampleInputEmail1">Username</label>
        <input type="username" name="username" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Username">
        <small id="emailHelp" class="form-text text-muted">Masukkan Username dengan benar.</small>
      </div>
      <div class="form-group">
        <label for="exampleInputPassword1">Password</label>
        <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
      </div>
      <div class="form-check">
        <input type="checkbox" class="form-check-input" id="exampleCheck1">
        <label class="form-check-label" for="exampleCheck1">Check me out</label>
      </div>
      <button type="submit" class="btn btn-primary">Submit</button>
    </form>
    <br>
    <?php echo validation_errors('<div class="error">', '</div>'); ?>
  </section>