<div class="container">
    <a href="<?= site_url('Register/index');?>" class="btn btn-success center-block">Tambah Data Siswa</a><br>
    <table class="table"
            id = "tablesiswa"    
            data-toggle="table" 
            data-search="true" 
            data-show-refresh="true"
            data-pagination="true"
            data-page-size="5"
            data-page-list="[5,10,All]"
            data-show-columns="true"
            data-show-toggle="true"
            data-side-pagination="server"
            data-card-view="false"
            data-detail-view="true"
            data-detail-formatter="detailFormatter"
            data-resizable="false"
            data-show-export="true"
            data-export-types="['pdf', 'csv', 'excel']"
            data-export-options='{"fileName": "data_siswa"}'
            data-click-to-select="true"
			data-url="<?php echo site_url('Siswa/data_siswa')?>">
        <thead>
            <tr>
                <th data-formatter="numberFormatter">No</th>
                <th data-field="nama">Nama</th>
                <th data-field="email">Email</th>
                <th data-field="no_telp">Nomor Telepon</th>
                <th data-field="jenis_kelamin">Jenis Kelamin</th>
                <th data-formatter="actionFormatter" data-field="id" data-force-hide="true">Aksi</th>           
            </tr>
        </thead>
    </table>
</div>
<script>
	function numberFormatter(value, row, index) {
	var options = $('#tablesiswa').bootstrapTable('getOptions')
	// alert(options["pageNumber"] + " " + options["pageSize"])
	//  console.log(options["pageSize"]);
	var tes = 0
	if (!isNaN(options['pageSize'])) { //cek pagesize angka atau tidak, klo angka jalankan kode dibawah
	tes = ((options["pageNumber"] - 1) * options["pageSize"])
	}
	return index + 1 + tes;
    }
    
	function actionFormatter(value, row, index){
		return[
			'<a href="<?= site_url("Siswa/detail/") ?>'+value+'" class="btn btn-primary ">Detail</a>',
			' ',
			'<a href="<?= site_url("Register/update/") ?>'+value+'" class="btn btn-success ">Edit</a>',
			' ',
			'<a href="<?= site_url("Siswa/delete/") ?>'+value+'" class="btn btn-danger"  >Hapus</a>',
		].join('');
	}

	function detailFormatter(index, row){
		var html = []
		const excludeType = ["Undefined","object"];
		$.each(row, function (key, value) {
			if(!excludeType.includes(typeof value)){
				html.push('<p><b>' + key + ':</b>' + value + '</p>')
			}
		})
		return html.join('')
	}

</script>