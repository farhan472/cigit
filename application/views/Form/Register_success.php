<div class="container">		
	<div class="row justify-content-center">
		<div class="col-md-6">	
			<table class="table table-responsive" style="text-align:center;">
				<tr>
					<td>Nama</td>
					<td>:</td>
					<td><?php echo $nama; ?></td>
				</tr>
				<tr>
					<td>Email</td>
					<td>:</td>
					<td><?php echo $email; ?></td>
				</tr>
				<tr>
					<td>Nomor telepon</td>
					<td>:</td>
					<td><?php echo $notelp; ?></td>
				</tr>
				<tr>
					<td>Tanggal Lahir</td>
					<td>:</td>
					<td><?php echo $tlahir; ?></td>
				</tr>
				<tr>
					<td>Jenis Kelamin</td>
					<td>:</td>
					<td><?php echo $jenis_kelamin; ?></td>
				</tr>
				<tr>
					<td>Agama</td>
					<td>:</td>
					<td><?php echo $agama; ?></td>
				</tr>
				<tr>
					<td>Alamat</td>
					<td>:</td>
					<td><?php echo $alamat; ?></td>
				</tr>
				<tr>
					<td>Tinggi</td>
					<td>:</td>
					<td><?php echo $tinggi; ?></td>
				</tr>
				<tr>
					<td>Berat</td>
					<td>:</td>
					<td><?php echo $berat; ?></td>
				</tr>
				<tr>
					<td>Berat badan Anda</td>
					<td>:</td>
					<td><?php echo $bmi; ?></td>
				</tr>
				<tr>
					<td>Foto</td>
					<td>:</td>
					<td>
					<a href="<?php echo site_url("register/download/$file_name/$orig_name"); ?>" target='_blank'><?php echo $orig_name?></a>
					<br><img src="<?php echo site_url("register/download/$file_name/$orig_name"); ?>" style="widht:200px;height:120px;">
					<!-- ada dua cara buat manggil parameter di link "<?php echo site_url("register/download/$file_name/$orig_name"); ?>" -->
																<!-- "<?php echo site_url('register/download/'.$file_name.'/'.$orig_name); ?>" -->
					</td>
				</tr>
				<tr>	
					<td><a href="<?= site_url('Login/delete');?>" class="btn btn-primary center-block">Logout</a></td>
					<td><a href="<?= site_url('Siswa/index');?>" class="btn btn-primary center-block">Daftar Siswa</a></td>
				</tr>
			</table>
		</div>
	</div>
</div>