<div class="container-fluid" style="text-align:center;">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Update Data Siswa</h3>
                </div>
                <div class="panel-body">
                    <form method="POST" action="<?php echo base_url('Register/update'); ?>" enctype="multipart/form-data">
                            <input name="id" type="hidden" value="<?php echo $siswa['id'];?>">
                        <div class="form-group">
                            <label>Nama</label>
                            <input type="text" name="nama" required class="form-control" placeholder="Enter Name" value="<?php echo $siswa['nama'];?>">
                            <small class="form-text text-danger"><?= form_error('nama');?></small>
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" name="email" required class="form-control" placeholder="Enter Email" value="<?php echo $siswa['email'];?>">
                            <small class="form-text text-danger"><?= form_error('email');?></small>
                        </div>
                        <div class="form-group">
                            <label>Nomor Telepon</label>
                            <input type="number" name="notelp" required class="form-control" placeholder="Enter No Telepon" value="<?php echo $siswa['no_telp'];?>">
                            <small class="form-text text-danger"><?= form_error('notelp');?></small>
                        </div>
                        <div class="form-group">
                            <label>Tanggal Lahir</label>
                            <input type="date" name="tlahir" required class="form-control" value="<?php echo $siswa['tanggal_lahir'];?>">
                            <small class="form-text text-danger"><?= form_error('tlahir');?></small>
                        </div>
                        <div class="form-group">
                            <label>Jenis Kelamin</label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="jenis_kelamin" value="laki-laki" required  <?php echo ($siswa['jenis_kelamin']=='laki-laki')?'checked':'' ?>>Laki-laki
                                    <input class="form-check-input" type="radio" name="jenis_kelamin" value="perempuan" required  <?php echo ($siswa['jenis_kelamin']=='perempuan')?'checked':'' ?>>Perempuan
                                <small class="form-text text-danger"><?= form_error('jenis_kelamin');?></small>
                        </div>
                        <div class="form-group">
                            <label>Agama</label>
                            <select class="form-control" name="agama" required>
                                <?php foreach ($agama as $a ) :?>
                                    <?php if($a == $siswa['agama']) :?>
                                        <option value="<?= $a;?>" selected><?= $a;?></option>
                                    <?php else :?>
                                        <option value="<?= $a;?>"><?= $a;?></option>
                                    <?php endif ;?>
                                <?php endforeach ; ?>
                            </select>
                            <small class="form-text text-danger"><?= form_error('agama');?></small>
                        </div>
                        <div class="form-group">
                            <label>Alamat</label>
                            <textarea name="alamat" required class="form-control"><?php echo $siswa['alamat'];?></textarea>
                            <small class="form-text text-danger"><?= form_error('alamat');?></small>
                        </div>
                        <div class="form-group">
                            <label>Tinggi</label>
                            <input type="number" required name="tinggi" class="form-control" placeholder="Tinggi" value="<?php echo $siswa['tinggi'];?>">
                            <small class="form-text text-danger"><?= form_error('tinggi');?></small>
                        </div>
                        <div class="form-group">
                            <label>Berat</label>
                            <input type="number" required name="berat" class="form-control" placeholder="berat" value="<?php echo $siswa['berat'];?>">
                            <small class="form-text text-danger"><?= form_error('berat');?></small>
                        </div> 
                        <div class="form-group">
                            <label>Tambahkan Foto Anda</label>
                            <input type="file" accept="image/*" name="file" class="form-control" value="<?php echo $siswa['foto']?>">
                        </div>
                        <button type="submit" class="btn btn-primary" name="update" value="update">Update</button>       
                        <br>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>