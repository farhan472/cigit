<div class="container-fluid" style="text-align:center;">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Update Data Siswa</h3>
                </div>
                <div class="panel-body">
                    <form method="POST" action="<?php echo site_url('Profil/edit_success'); ?>" enctype="multipart/form-data">
                            <input name="id" type="hidden" value="<?php echo $id;?>">
                            <input name="password" type="hidden" value="<?php echo $password;?>">
                        <div class="form-group">
                            <label>Username</label>
                            <input type="text" name="username" required class="form-control" placeholder="Enter Name" value="<?php echo $username;?>">
                            <small class="form-text text-danger"><?= form_error('nama');?></small>
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" name="email" required class="form-control" placeholder="Enter Email" value="<?php echo $email;?>">
                            <small class="form-text text-danger"><?= form_error('email');?></small>
                        </div>
                        <div class="form-group">
                            <label>Tambahkan Foto Anda</label>
                            <input type="file" accept="image/*" name="foto" class="form-control" value="<?php echo $foto?>">
                        </div>
                        <button type="submit" class="btn btn-primary" name="update" value="update">Update</button>       
                        <br>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>