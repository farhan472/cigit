<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>{judul}</title>
  </head>
  <body style="background-color:white;">
    
    <header style="
      background-color: grey;
      width: 100%;
      height: 70px;
      padding-top:1px;">
        <nav style="
          font-family: Arial, Helvetica, sans-serif;
          color: #276097;">
            <h1 style="text-align:center;">ADW Magazine</h1>
        </nav>
    </header>
    <section style="
      height: 100%;
      width: 95%;
      margin-left:20px;
      margin-right:15px;">
    <h1>Hey,{nama}</h1>
    <p>{message}</p>
    <img src="{gambar}" alt="" style="width:50%">
    <p style="width:100%;text-align:center;">Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae dolores delectus atque suscipit reprehenderit! In voluptas consectetur, quidem quas voluptatem consequatur repellendus dolorem eos dolor, iure ut enim cumque blanditiis?</p>
    <p style="width:100%;text-align:center;">Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae dolores delectus atque suscipit reprehenderit! In voluptas consectetur, quidem quas voluptatem consequatur repellendus dolorem eos dolor, iure ut enim cumque blanditiis?</p>
    <p style="width:100%;text-align:center;">Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae dolores delectus atque suscipit reprehenderit! In voluptas consectetur, quidem quas voluptatem consequatur repellendus dolorem eos dolor, iure ut enim cumque blanditiis?</p>
    </section>

    <footer style="
      background-color: grey;
      height: 60px;
      font-size: 14px;
      box-shadow: 0 -5px 8px rgba(0, 0, 0, 0.2);
      color: white;
      font-family: sans-serif, Helvetica ,Arial ;
      text-align:center;
      padding-top:7px;">
        <p style="text-align:center;">Jalan Ciracas,Jakarta Timur</p>
        <br>
    </footer>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  </body>
</html>