<!DOCTYPE html>
<html>
<head>
	<title>Sistem Akademik Sederhana | SIS ANA</title>
	<!-- Latest compiled and minified CSS -->
	<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->

	<!-- Optional theme -->
	<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous"> -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

</head>
<body>
<div style="padding: 10px">
<h1>Sistem Akademik Sederhana</h1>

<ul>
	<li><a href="<?php echo site_url() ?>">Home</a></li>
	<li><a href="<?php echo site_url('jurusan') ?>">Lihat Jurusan </a></li>
	<li><a href="<?php echo site_url('jurusan/tambah') ?>">Tambah Jurusan</a></li>
	<li><a href="<?php echo site_url('kelas') ?>">Lihat Kelas</a></li>
	<li><a href="<?php echo site_url('kelas/form') ?>">Tambah Kelas</a></li>
</ul>